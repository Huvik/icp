#ifndef consolePresenter_H
#define consolePresenter_H

#include <iostream>
#include <string>
#include <thread>

#include "clientCommunication.h"

class consolePresenter
{
	private:
		clientCommunication *cc;
	public:
		consolePresenter(std::string host, std::string port);
		~consolePresenter();
		void printMenu();
		int checkCommand(std::string command);
		void printGames(int numOfGames, gameInfo *games);
		void start();
		void printMaps(int numOfMaps, mapsInfoSend *maps);
		void listenPrint();
		void readCommands();
};

#endif
