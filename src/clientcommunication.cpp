#include "clientcommunication.h"

/**
 * Pri spusteni bez parametru se nastavi defaultni hodnoty
 * Host: localhost
 * Port: 5000
 **/
clientCommunication::clientCommunication()
{
    connect("localhost", "5000");
}

/**
 * Pri spusteni se dvema parametry dojde k pokusu o pripojeni s temito udaji
 * @param ip Host
 * @param port Port
 **/
clientCommunication::clientCommunication(std::string ip, std::string port)
{
    connect(ip, port);
}

/**
 * Pri ruseni se uvolni dynamicky alokovana pamet
 **/
clientCommunication::~clientCommunication()
{
    delete socket;
    delete receiver_endpoint;
}

/**
 * Pokusi se spojit se serverem podle vstupnich parametru
 * @param ip Hostitelsky server
 * @param port Port, na kterem snad posloucha
 **/
void clientCommunication::connect(std::string ip, std::string port)
{
	try
	{
		// Vytvorime semafory pro kontrolu nad odesilanymi daty serveru
        	key_t key = ftok("icpCC", 1);
        	sem_id = semget(key, 1, IPC_CREAT | IPC_EXCL | 0666);

		// Nastavime Signal a Wait
        	semSet();

        	gameID = 0;

        	// Zpristupneni udp
        	using boost::asio::ip::udp;

        	// Vytvoreni spojeni
        	boost::asio::io_service io_service;

        	udp::resolver resolver(io_service);
        	udp::resolver::query query(udp::v4(), ip, port);
        	receiver_endpoint = new udp::endpoint(*resolver.resolve(query));

        	socket = new udp::socket(io_service);
        	socket->open(udp::v4());
	}
	// Odchytavani pripadnych vyjimek
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
}

/**
 * Ziska od serveru nove ID pro uzivatele, aby se mel v budoucnu cim prokazovat a neco si i zahral
 **/
void clientCommunication::getID()
{
	try
	{
        	// Nastaveni prvotni zpravy
        	clientMessage clm;
        	clm.message = NewPlayer;

		// Odeslani zadosti
        	socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);

        	// Nacteni odpovedi a ulozeni ID hrace
        	socket->receive_from(boost::asio::buffer(&playerID, sizeof(int)), *receiver_endpoint);
        	std::cout << playerID << std::endl;
	}
	// Pokud by nastala nejaka ta vyjimka
	catch (std::exception& e)
	{
	        std::cerr << e.what() << std::endl;
	}
}
/**
 * Podle zadaneho ID se pokusi pripojit k jiz rozehrane hre
 * @param gameID ID hry ke ktere se zkousime pripojit
 * @return Uspesnost pripojeni - True - jsme ve hre a muzeme hrat
 *                             - False - hra je jiz plna, nebo neexistuje
 **/
bool clientCommunication::joinGame(std::string gameID)
{
	char *ptr;
	return joinGame(strtol(gameID.c_str(), &ptr, 10));
}

/**
 * Podle zadaneho ID se pokusi pripojit k jiz rozehrane hre
 * @param gameID ID hry ke ktere se zkousime pripojit
 * @return Uspesnost pripojeni - True - jsme ve hre a muzeme hrat
 *                             - False - hra je jiz plna, nebo neexistuje
 **/
bool clientCommunication::joinGame(int gameID)
{
	try
	{
		char *ptr;

		// Nastaveni zpravi s zadosti o pripojeni
		clientMessage clm;
		clm.message = JoinGame;
		clm.playerID = this->playerID;
		// Pretypovani ID
		clm.gameID = gameID;

		// Odeslani zadosti
		socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);

		// Cekame na odpoved
		socket->receive_from(boost::asio::buffer(&(this->gameID), sizeof(int)), *receiver_endpoint);

		// Pokud se nam nenastavilo ID hry, nepodarilo se nam to
		if(this->gameID == 0)
			return false;

		// Jinak ano
		return true;
	}
	// Kdyby nahodou
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return false;
	}
}

/**
 * Nacte jiz rozehrane hry
 * @param games Ukazatel na misto, kam se hry nactou
 * @param numOfGames Pocet rozehranych her
 **/
void clientCommunication::loadGames(gameInfo **games, int *numOfGames)
{
	try
	{
		// Vymazani predchozich her (pokud chtel refresh)
		delete *games;
		*games = NULL;

		// Nastaveni zadosti
		clientMessage clm;
		clm.message = SendGames;

		// Odeslani zadosti
		socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);

		// Nacte pocet her na serveru
		socket->receive_from(boost::asio::buffer(numOfGames, sizeof(int)), *receiver_endpoint);

		// Pokud jich je 0, mame smulu
		if(*numOfGames == 0)
			return;

		// Alokace mista pro inforamce o hrach
		*games = new gameInfo[*numOfGames];

		// Nacteni jednotlivych her
		socket->receive_from(boost::asio::buffer(*games, sizeof(gameInfo) * (*numOfGames)), *receiver_endpoint);
	}
	// Odchytani vyjimky
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
}

/**
 * Nacteni vsech hratelnych map ze serveru
 * @param maps Ukazatel na strukturu s informacemi o mapach
 * @param numOfMaps Pocet map
 **/
void clientCommunication::loadMaps(mapsInfoSend **maps, int *numOfMaps)
{
	try
	{
		// Smazeme predchozi mapy (pokud je chtel nacist znovu)
		delete *maps;
		*maps = NULL;

		// Zadost o zaslani map
		clientMessage clm;
		clm.message = SendMaps;

		// Odesleme zadost
		socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);

		// Pokud jsme dostali kladne cislo, mapy existuji!
		socket->receive_from(boost::asio::buffer(numOfMaps, sizeof(int)), *receiver_endpoint);

		// Jinak se vratim s prazdnou :(
		if(*numOfMaps == 0)
			return;

		// Vytvorime prostor s dostatecnym mistem
		*maps = new mapsInfoSend[*numOfMaps];

		// Nacteme mapy
		socket->receive_from(boost::asio::buffer(*maps, sizeof(mapsInfoSend) * (*numOfMaps)), *receiver_endpoint);
	}
	// Odchytavani vyjimek
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
}

/**
 * Pretypuje parametry a zavola funkci na vytvoreni nove hry
 **/
bool clientCommunication::createGame(std::string mapID, std::string step)
{
	char *ptr;
	double pom = std::atof(step.c_str());
	if(pom > 5 || pom < 0.5)
		return false;
	else
		pom *= 1000;
	return createGame(strtol(mapID.c_str(), &ptr, 10), (int)pom);
}

/**
 * Zados o vytvoreni nove hry
 * @param mapID Nova hra se bude hrat na mape s timto ID
 * @return Uspesnost vytvoreni - True - jsme ve hre a muzeme hrat
 *                             - False - hra je jiz plna, nebo neexistuje
 **/
bool clientCommunication::createGame(int mapID, int step)
{
	try
	{
		// Nastaveni zadosti o vytvoreni nove hry
		clientMessage clm;
		clm.message = CreateGame;

		// Nastaveni mapy
		clm.gameID = mapID;
		clm.step = step;
		clm.playerID = this->playerID;

		// Odeslani zadosti
		socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);

		// Prijeni ID hry
		socket->receive_from(boost::asio::buffer(&gameID, sizeof(int)), *receiver_endpoint);

		// Pokud jsme obdrzeli 0, pak se vytvoreni nepovedlo
		if(gameID == 0)
			return false;

		// Jinak muzeme hrat
		return true;
	}
	// Odchytavani vyjimek
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return false;
	}
}

/**
 * Nacitani zprav ohledne stavu hry. Jelikoz zaroven muzeme odesilat prikazy, pojistime si exkluzivitu pomoci semaforu
 * @param gp Herni plan s informacemi
 **/
void clientCommunication::listenGame(gameplan *gp)
{
	// Uzamknuti
	semop(sem_id, &semWait, 0);

	// Cekani na stav hry
	socket->receive_from(boost::asio::buffer(gp, sizeof(gameplan)), *receiver_endpoint);

	// Pokud se neco napodarilo
	if(gp->msg != Error && gameID == 0)
		gameID = gp->gameID;

	// Odemknuti
	semop(sem_id, &semSignal, 0);
}

/**
 * Chceme-li odeslat prikaz, overime, zda-li vubec existuje
 * @param command Prikaz k odeslani
 **/
void clientCommunication::sendCommand(std::string command)
{
	// Nastaveni zpravy s prikazem
	clientMessage clm;
	clm.message;
	clm.playerID = this->playerID;
	clm.gameID = this->gameID;

	// Zjisteni a prevedeni prikazu
	if(!command.compare("go"))
		clm.message = Go;
	else if(!command.compare("step"))
		clm.message = Step;
	else if(!command.compare("keys"))
		clm.message = Keys;
	else if(!command.compare("stop"))
		clm.message = Stop;
	else if(!command.compare("left"))
		clm.message = Left;
	else if(!command.compare("right"))
		clm.message = Right;
	else if(!command.compare("take"))
		clm.message = Take;
	else if(!command.compare("open"))
		clm.message = Open;
	else
		return;
	// Uzamceni	
	semop(sem_id, &semWait, 0);

	// Odeslani
	socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);

	// Odemknuti
	semop(sem_id, &semSignal, 0);
}

/**
 * Funkce, ktera nastavi menici signaly semaforu.
 * @param var Informacni struktura se vsemi dulezetymi informacemi
 * @param semNum Cislo udavajici pocet semaforu
 **/
void clientCommunication::semSet()
{
    semctl(sem_id, 0, SETVAL, 0);

    semWait.sem_num = 0;
    semWait.sem_op = -1;
    semWait.sem_flg = 0;

    semSignal.sem_num = 0;
    semSignal.sem_op = 1;
    semSignal.sem_flg = 0;
}

