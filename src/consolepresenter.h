/**
 * Projekt Bludiste do predmetu ICP 2013/2014
 * Authors: Jan Trcka - xtrcka07 (xtrcka07@stud.fit.vutbr.cz, trcka.j@gmail.com)
 *          Lukas Huvar - xhuvar00 (xhuvar00@stud.fit.vutbr.cz)
 * Date: 11. 05. 2014
 * Description: Trida majici na starosti zobrazovani dat ze serveru a komunikaci s uzivatelem
 **/


#ifndef consolePresenter_H
#define consolePresenter_H

#include <iostream>
#include <string>
#include <thread>

#include "clientcommunication.h"

class consolePresenter
{
	private:
		// Ukazatel na komunikacti tridu
		clientCommunication *cc;
	public:
		// Konstruktory a destruktory
		consolePresenter(std::string host, std::string port);
		~consolePresenter();

		// Ostatni funkce
		void printMenu();
		int checkCommand(std::string command);
		void printGames(int numOfGames, gameInfo *games);
		void start();
		void printMaps(int numOfMaps, mapsInfoSend *maps);
		void listenPrint();
		void readCommands();
};

#endif
