/**
 * Projekt Bludiste do predmetu ICP 2013/2014
 * Authors: Jan Trcka - xtrcka07 (xtrcka07@stud.fit.vutbr.cz, trcka.j@gmail.com)
 *          Lukas Huvar - xhuvar00 (xhuvar00@stud.fit.vutbr.cz)
 * Date: 11. 05. 2014
 * Description: Main pro consolovou aplikaci
 **/
#include <iostream>

#include "consolepresenter.h"

/**
 * Zacatek consolove aplikace. Podle vstupnich parametru bud doda defaultni, ze vstupu, nebo vypise info
 * Pokud se vse zdarilo, odstratuje obsluhu.
 **/
int main(int argc, char **argv)
{
	consolePresenter *cp;
	if(argc == 1)
		cp = new consolePresenter("localhost", "5000");
	else if(argc == 3)
		cp = new consolePresenter(argv[1], argv[2]);
	else {
		std::cerr << "Usage: client <server_address> <port>" << std::endl;
		std::cerr << "       client - with default settings (localhost, 5000)" << std::endl;
		exit(1);
	}

	cp->start();

	return 0;
}
