#include "basic_objects.hpp"


#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include <thread> 

/* Vraci EMPTY, pokud je policko prazdne
 * Vraci OCCUPIED, pokud je na policku hrac nebo zavrena brana/zed
 * Vraci DEAD, pokud hrac chce vejit na policko
 */
move_result maze_cell::empty()
{
	if(active_object == nullptr)
	{
		if(accessible)
			return EMPTY;
		else
			return OCCUPIED;
	}
	else  
	{
		if(active_object->type() == POLICEMAN)
			return DEAD;
		else
			return OCCUPIED;
	}
}




// Konstruktor pro prvek pole
maze_cell::maze_cell (bool accessible, maze_cell_type type, person *active_object)
{
	this->accessible = accessible;
	this->type = type;
	this->active_object = active_object;
}

game_plan::game_plan(unsigned int width, unsigned int height, std::ifstream &map)
{
	this->width = width;
	this->height = height;

	unsigned char c;

	maze = new maze_cell** [width];
	for(int x = 0; x < width; x++)
		maze[x] = new maze_cell* [height];

	for (int x = 0; x < width; ++x)
	{
		for (int y = 0; y < height; ++y)
		{
			map >> c;
			if(c == '#')
				maze[x][y] = new maze_cell(false, WALL, nullptr);
			if(c == '.')
				maze[x][y] = new maze_cell(true, PATH, nullptr);
			if(c == 'F')
			{
				end_game = new finish(true, FINISH, nullptr);
				maze[x][y] = end_game;
			}
			if(c == 'K')
				maze[x][y] = new key(true, KEY, nullptr);
			if(c == 'G')
				maze[x][y] = new gate(false, GATE_CLOSED, nullptr);
			if(c == 'P')
			{
				policeman *n = new policeman(x,y);
				maze[x][y] = new maze_cell(true, PATH, n);
   			police.push_back(n);
			}
		}
	}
}


/*
 const int max_size = 50;
 const int min_size = 20;

 int main(int argc, char const *argv[])
 {
 	std::ifstream game_map;
	//open map 
 	game_map.open("./examples/20x20");
 	if(!game_map)
 	{
 		std::cerr << "Cant open game_map" << std::endl;
 		return -1;
 	}
 	std::string line;
	//read first line with size of map
 	std::getline(game_map, line);
 	int Row,Column;
 	char x;
	//parse first line to size of map
 	std::istringstream iss(line);
 	iss >> Row >> x >> Column;
 	if( Row > max_size || Row < min_size || Column > max_size || Column < min_size)
 	{
 		std::cerr << "Bad size of map" << std::endl;
 		return -1;
 	}

 	game_plan new_game(Row,Column, game_map);

 		for (int y = 0; y < Row; ++y)
 		{
 			for (int x = 0; x < Column; ++x)
 			{
 				if(new_game.test(y,x) == WALL)
 					std::cout << "#";
 				if(new_game.test(y,x) == PATH)
 					std::cout << ".";
 				if(new_game.test(y,x) == FINISH)
 					std::cout << "F";
 				if(new_game.test(y,x) == GATE_CLOSED)
 					std::cout << "G";
 				if(new_game.test(y,x) == KEY)
 					std::cout << "K";
 				if(new_game.test(y,x) == PERSON)
 				{
 					person *n = new_game.type_player(y,x);
 					if(n->type() == POLICEMAN)
 					{
 						orientation rot = n->ret_orientation();
 						if(rot == UP)
 							std::cout << "^";
 						if(rot == DOWN)
 							std::cout << "v";
 						if(rot == LEFT)
 							std::cout << "<";
 						if(rot == RIGHT)
 							std::cout << ">";
 					}
 				}
 			}
 			std::cout << std::endl;
 		}
 

 	return 0;
 }
*/
