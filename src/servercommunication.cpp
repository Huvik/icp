/**
 * Projekt Bludiste do predmetu ICP 2013/2014
 * Authors: Jan Trcka - xtrcka07 (xtrcka07@stud.fit.vutbr.cz, trcka.j@gmail.com)
 *          Lukas Huvar - xhuvar00 (xhuvar00@stud.fit.vutbr.cz)
 * Date: 11. 05. 2014
 * Description: Trida pro obsluhu a komunikaci ze strany serveru
 **/

#include "servercommunication.h"

/**
 * Pocatecni vlakno starajici se vytvoreni nove hry
 * @param sender_endpoint Endpoint na urciteho klienta
 * @param sock Nastroj pro komunikaci s klientem
 * @param clm Zprava od clienta zakladajici novou hru
 **/
void serverCommunication::newThr(boost::asio::ip::udp::endpoint sender_endpoint, boost::asio::ip::udp::socket *sock, clientMessage clm)
{
	// Uzamkneme vektory her
	semop(sem_id, &semWait, 0);

	// Vytvorime nove casti hry podle informaci od uzivatele
	mapsInfo mi;
	mi = maps->at(clm.gameID);

	gameInfo gi;
	gi.gameID = pomGameID++;
	gi.numOfPlayers = 1;
	gi.gameTime = time(0);	

	// Ulozime novou hru do seznamu hranych her
	games->push_back(gi);

	// Pokracujeme k dukladnejsi tvorbe
	int gameID = gl->createGame(gi, mi, sender_endpoint, clm.playerID);

	// Uvolnime vektory her
	semop(sem_id, &semSignal, 0);

	// Pokud se neco nepovedlo, ukoncime to
	if(gameID == 0)
		return;

	// Jinak jedeme dokola
	while(true)
	{
		// Uspime vlakno na krok
		std::this_thread::sleep_for(std::chrono::milliseconds(clm.step));

		// Zabereme vektory her
		semop(sem_id, &semWait, 0);

		// Vypocitame zmeny
		gl->playGame(gameID);

		// Rozecleme nove udaje, pripadne ukoncime cykleni (konec/chyba)
		if(!sendGame(sock, gameID))
			break;

		// Uvolnime vektory
		semop(sem_id, &semSignal, 0);
	}

	// Vyhledame hru ve bektorech a vymazeme ji
	for(int i = 0; i < gl->glGames->size(); i++) {
		if((gl->glGames->at(i))->gameID == gameID) {
			gl->glGames->erase(gl->glGames->begin()+i);
		}
	}

	for(int i = 0; i < games->size(); i++) {
		if((games->at(i)).gameID == gameID) {
			this->games->erase(this->games->begin()+i);
		}
	}	

	// Uvolnime vektory her
	semop(sem_id, &semSignal, 0);
}

/**
 * Smycka starajici se o vsechny klienty.
 * @param Port na kterem se ma poslouchat
 **/
serverCommunication::serverCommunication(std::string port)
{
	// Vytvoreni semaforu k vectorum
	key_t key = ftok("icp", 1);
	sem_id = semget(key, 1, IPC_CREAT | IPC_EXCL | 0666);

	// Vytvoreni zakladnich prommenych
	semSet();
	pomPlayerID = 1;
	pomGameID = 1;
	gl = new gameLogic();
	games = new std::vector<gameInfo>();
	maps = new std::vector<mapsInfo>();

	// Nacteni map
	loadMaps();

	// Pomocny ukazatel na vlakno, ktere se bude oddelovat u novych her
	std::thread *connectionThread;

	using boost::asio::ip::udp;
	try {
		// Vytvorime server na zadanem portu
		boost::asio::io_service io_service;
		udp::socket sock(io_service, udp::endpoint(udp::v4(), atoi(port.c_str())));

		// Promenne pro clienta
		clientMessage clm;
		udp::endpoint sender_endpoint;
		while (true) {

			// Uzavreme vektory
			semop(sem_id, &semWait, 0);

			// Zacneme s obsluhou clienta
			size_t length = sock.receive_from(boost::asio::buffer(&clm, sizeof(clientMessage)), sender_endpoint);
			int pom;

			// Copak nam asi poslal za zpravu
			switch(clm.message)
			{
				// Novy client, zvysime ID a odesleme mu jeho
				case NewPlayer: 
						pom = pomPlayerID++;
						sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
					break;
				// Zadost o hry
				case SendGames:
						// Odesleme mu pocet
						pom = games->size();
						sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);

						// Pokud nejake jsou tak si je presuneme mimo a odesleme mu je
						if(pom != 0) 
						{
							gameInfo *g = new gameInfo[pom];
							for(int i = 0; i < pom; i++)
							{
								g[i] = games->at(i);
								g[i].gameTime = time(0) - g[i].gameTime;
							}						

							sock.send_to(boost::asio::buffer(g, sizeof(gameInfo) * pom), sender_endpoint);
							delete g;
						}
				

					break;
				// Zadost o mapy
				case SendMaps:	
						// Odesleme mu pocet					
						pom = maps->size();
						sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);

						// Pokud nejake jsou, tak si je zkopirujeme mimo a odesleme
						if(pom != 0) 
						{
							mapsInfoSend *mi = new mapsInfoSend[pom];

							for(int i = 0; i < pom; i++){
								mi[i].width = maps->at(i).width;
								mi[i].height = maps->at(i).height;
								mi[i].mazeID = maps->at(i).mazeID;
							}		

							sock.send_to(boost::asio::buffer(mi, sizeof(mapsInfoSend) * pom), sender_endpoint);
							//delete mi;
						}
					break;
				// Zalozeni nove hry
				case CreateGame:
						pom = false;
						// Pokud vybrana mapa existuje
						if(clm.gameID < maps->size()) {
							// Zalozime hru v novem vlakne
							pom = true;
							sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
							connectionThread = new std::thread(&serverCommunication::newThr, this, sender_endpoint, &sock, clm);
						}
						// Jinak vypiseme chybu
						else
							sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
					break;
				// Pripojeni ke hre
				case JoinGame:
						pom = false;
						// Vyhledame jeho vybranou hru
						for(int i = 0; i < gl->glGames->size(); i++) {
							// Pokud existuje a je tam misto
							if(clm.gameID == gl->glGames->at(i)->gameID)	{
								if(gl->glGames->at(i)->numOfPlayers < 4)
								{
									// Pridame jej do hracu
									gl->glGames->at(i)->players[gl->glGames->at(i)->numOfPlayers].setEndpoint(sender_endpoint);
									//gl->glGames->at(i)->players[pocet].setEndpoint(sender_endpoint);
									// Pridani hrace na hraci plochu
									int width = gl->glGames->at(i)->width;
									int height = gl->glGames->at(i)->height;
									gl->glGames->at(i)->players[gl->glGames->at(i)->numOfPlayers].joinGame(clm.playerID, static_cast<players>(gl->glGames->at(i)->numOfPlayers), width-2, height-2, gl->glGames->at(i)->gameLogic->m());
									gl->glGames->at(i)->numOfPlayers++;
									for(int a = 0; a < games->size(); a++) {
										if(clm.gameID == games->at(i).gameID)
											games->at(i).numOfPlayers++;
									}
									pom = gl->glGames->at(i)->gameID;
									sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
								}
								else
									sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
							}
							if(i+1 == gl->glGames->size())
								sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
						}
					break;
				// Obsluha prikazu
				case Go:{
					player *p = getPlayer(clm);
					if(p == NULL)
						exit(1);
					p->go();
					sendGame(&sock,clm.gameID);
					}
					break;
				case Step: {
					player *p = getPlayer(clm);
					if(p == NULL)
						exit(1);
					p->step();
					sendGame(&sock,clm.gameID);
					}
					break;
				case Stop: {
					player *p = getPlayer(clm);
					if(p == NULL)
						exit(1);
					p->stop();
					sendGame(&sock,clm.gameID);
					}
					break;
				case Left: {
					player *p = getPlayer(clm);
					if(p == NULL)
						exit(1);
					p->left();
					sendGame(&sock,clm.gameID);
					}
					break;								
				case Right: {
					player *p = getPlayer(clm);
					if(p == NULL)
						exit(1);
					p->right();
					sendGame(&sock,clm.gameID);
					}
					break;
				case Take: {
					player *p = getPlayer(clm);
					if(p == NULL)
						exit(1);
					p->pickKey();
					sendGame(&sock,clm.gameID);
					}
				case Open: {
					player *p = getPlayer(clm);
					if(p == NULL)
						exit(1);
					p->openGate();
					sendGame(&sock,clm.gameID);
					}
					
				case Keys: {
					player *p = getPlayer(clm);
					if(p == NULL)
						exit(1);
					int pocet = p->retKeys();
					//TODO nekam to nacpat a poslat? + vsechny ostatni zpravy o uspesnosti operace
					}
					break;
				default:
					break;
			}
			semop(sem_id, &semSignal, 0);
		}
	} 
	catch (std::exception& e) 
	{
        	std::cerr << e.what() << std::endl;
    	}
}

/**
 * Podle zadanych udaju ID hry a ID uzivatel, vyhleda uzivatele v dane hre a vrati ukazetel na nej
 * @param msg Zprava od clienta
 * @return Ukazatel na uzivatele
 **/
player* serverCommunication::getPlayer(clientMessage msg)
{
	// Projdeme vsechny hry
	for(int i = 0; i < gl->glGames->size(); i++)
	{
		// Pokud narazime na hru, kterou chceme
		if(msg.gameID == gl->glGames->at(i)->gameID)
		{
			// Projdeme jeji hrace
			for(int a = 0; a < gl->glGames->at(i)->numOfPlayers; a++)
			{
				// Pokud je to on, vratime ukazatel na nej
				if(gl->glGames->at(i)->players[a].retPlayerid() == msg.playerID)
					return &gl->glGames->at(i)->players[a];
			}
		}
	}
	return NULL;
}

/**
 * Vezme hru, kterou rozpozna podle gameID a jeji aktualni stav rozesle vsem jejim hracum
 * @param sock Socket ke komunikaci ze strany serveru
 * @param gameID Hra, ktera se ma rozeslat
 * @return Pokud nastal konec hry, vrati se false
 **/
bool serverCommunication::sendGame(boost::asio::ip::udp::socket *sock, int gameID)
{
	// poradi hry ve vektoru
	int i;

	// Pokud zadne hry nejsou, vratime false
	if(gl->glGames->size() == 0)
		return false;

	// Projdeme hry a vyhledame nase ID
	for(i = 0; i < gl->glGames->size(); i++){
		if(gl->glGames->at(i)->gameID == gameID)
			break;
		if(i+1 >= gl->glGames->size())
			return false;
	}

	// Hru si ulozime zvlast (jen pro pohodli vypostu)
	gameplan pom = *gl->glGames->at(i);

	// Nacte stavajici stav hry
	pom.gameLogic->createMap(pom.maze);

	// Odesle jej vsem hracum
	for(i = 0; i < pom.numOfPlayers; i++)
	{
		sock->send_to(boost::asio::buffer(&pom, sizeof(gameplan)), pom.players[i].retEndpoint());
	}
	if(pom.msg == Error)
		return false;
		
	return true;
}

/**
 * Na zacatku cinnosti serveru se nactou vsechny mapy ze slozky a zakladni informace se ulozi do pomocneho pole
 **/
void serverCommunication::loadMaps()
{
	// Otevreni slozky s mapama
	DIR *dp;
	int i = 0;
	struct dirent *ep;     
	dp = opendir ("./examples/");

	// Pokud se mi podrilo otevrit slozku
	if (dp != NULL)
	{
		// Projdu vsechny soubory
		while (ep = readdir (dp)) 
		{
			// Pokud se nejdena o aktualni slozku, nebo nadrezenou
			if(strcmp(".", ep->d_name) && strcmp("..", ep->d_name))
			{
				// Pomocne promene
				mapsInfo mi;
				mi.mazeID = i;
				i++;

				// Otevreni mapy a ulozeni jmena
				std::ifstream gameMap; 
				std::string ss;
				ss = "./examples/";
				ss.append(ep->d_name);

			 	gameMap.open(ss.c_str());
			 	if(!gameMap)
			 	{
			 		std::cerr << "Cant open gameMap" << ep->d_name << std::endl;
			 		continue;
			 	}
			 	std::string line;

				// Nacteni rozmeru mapy
			 	std::getline(gameMap, line);
			 	int row, column;
			 	char x;

				// Rozparsovani rozmeru
			 	std::istringstream iss(line);
			 	iss >> row >> x >> column;

				// Pokud nevyhovuje, jdeme na dalsi
			 	if( row > 50 || row < 20 || column > 50 || column < 20)
			 	{
			 		std::cerr << "Bad size of map" << ep->d_name << std::endl;
					gameMap.close();
			 		continue;
			 	}
				gameMap.close();

				// Jinak si udaje o ni zapiseme
				mi.width = column;
				mi.height = row;
				mi.mapName = ss;
				maps->push_back(mi);
			}
		}
		(void) closedir (dp);
		std::cout << "There is " << maps->size() << " maps" << std::endl;
	}
	else
		std::cerr << "Couldn't open the directory" << std::endl;
}

/**
 * Funkce, ktera nastavi menici signaly semaforu.
 **/
void serverCommunication::semSet()
{
	semctl(sem_id, 0, SETVAL, 0);

	semWait.sem_num = 0;
	semWait.sem_op = -1;
	semWait.sem_flg = 0;

	semSignal.sem_num = 0;
	semSignal.sem_op = 1;
	semSignal.sem_flg = 0;
}
