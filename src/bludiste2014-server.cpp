/**
 * Projekt Bludiste do predmetu ICP 2013/2014
 * Authors: Jan Trcka - xtrcka07 (xtrcka07@stud.fit.vutbr.cz, trcka.j@gmail.com)
 *          Lukas Huvar - xhuvar00 (xhuvar00@stud.fit.vutbr.cz)
 * Date: 11. 05. 2014
 * Description: Main pro serverovou aplikaci
 **/

#include <iostream>

#include "servercommunication.h"

/**
 * Zacatek serverove aplikace. Podle vstupnich parametru bud doda defaultni, ze vstupu, nebo vypise info
 * Pokud se vse zdarilo, odstratuje obsluhu.
 **/
int main(int argc, char **argv)
{
	serverCommunication *sc;
	if(argc == 1)
		sc = new serverCommunication("5000");
	else if(argc == 3)
		sc = new serverCommunication(argv[1]);
	else {
		std::cerr << "Usage: server <port>" << std::endl;
		std::cerr << "       server - with default settings (5000)" << std::endl;
		exit(1);
	}
	return 0;
}
