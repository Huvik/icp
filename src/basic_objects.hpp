#ifndef basic_objects_H
#define basic_objects_H

#include <vector>
#include <random>
#include <cmath>
#include <chrono>


// Typy prvku bludiste
enum maze_cell_type {
		WALL,		// Zed
		PATH,		// Cesta
		GATE_CLOSED,	// Uzavrena brana
		FINISH,		// Cil
		KEY,		// Klic
		PERSON // Hrac nebo Policie
		};

// Orientace pohyblivych prvku
enum orientation { UP, DOWN, LEFT, RIGHT};

// Zpravy, ktere muze poslat server a on se na zaklade toho rozhodne co delat
enum clientMsg {NewPlayer};

/**
 * Pokud je policko volne vraci se empty
 * Pokud je policko obsazene vraci se occupied
 * Pokud stoji na policku policie a vejde hrac vraci se dead
 */
enum move_result {EMPTY, OCCUPIED, DEAD};

enum players{RED, BLUE, GREEN, YELLOW, POLICEMAN};


class person
{
	protected:
		unsigned int x; // x souradnice
		unsigned int y; // y souradnice
		orientation rotation;		// Natoceni postavy
		players id;
	public:
		person(int x,int y)
		{
			this->x = x;
			this->y = y;
		}
		virtual players type(){};

		orientation ret_orientation()
		{
			return rotation;
		}
};

class player: public person
{
	public:
		int numOfKeys;		// Pocet klicu
};

class policeman: public person
{
	public: 	
	
	virtual players type()
	{
		return id;
	}

	policeman(int x,int y) :person(x,y)
	{
		this->id = POLICEMAN;
  	std::random_device random;
  	std::default_random_engine engine(random());
  	std::uniform_int_distribution<int> uniform_dist(0, 3);
  	rotation = static_cast<orientation>(uniform_dist(engine));
	}
};


/**
 * Trida pro herni prvky planu rodic 
 * Cesta nebo Zed jinak 
 */
class maze_cell
{
	protected:
		person *active_object;	//aktivni prvek na policku 
		maze_cell_type type;	// Typ pole (jiz primo o co se jedna)
		bool accessible;	// Prostupnost pole
	public:

		/**
		 * Vraci hodnotu policka pro cestu je volna
		 */
		virtual move_result empty();
		/**
		 * Vraci hodnotu true pokud jde vzit klic jinak false
		 */
		virtual bool take_key()
		{
			return false;
		}
		/**
		 * Vraci hodnotu true pokud jde brana otevrit jinak false
		 */
		virtual bool open_gate()
		{
			return false;
		}

		/**
		 * Bunka nebo na ni stoji hrac
		 */
		virtual maze_cell_type cell_type()
		{
			if(active_object == nullptr)
				return type;
			else
				return PERSON;
		}

		/*
		 * Vraci adresu hrace pro vykresleni
		 */
		person *type_player()
		{
			return active_object;
		}
		//Konstruktor
		maze_cell(bool, maze_cell_type, person*);
		//Destruktor
		~maze_cell()
		{
			active_object = nullptr;
		}
};

/**
 * Trida pro branu
 */
class gate: public maze_cell
{
	private:
		bool open;
	public:
	// Pokud je brana zavrena nejde vstoupit
	virtual move_result empty()
	{
		if (open)
			return EMPTY;
		else
			return OCCUPIED;
	}
	// Pokud je brana zavrena tvari se jako brana jinak jako volne pole
	virtual maze_cell_type cell_type()
	{
		if(active_object != nullptr)
			return PERSON;
		else if(open)
			return PATH;
		else
			return GATE_CLOSED;
	}
	// Otevreni brany
	virtual bool open_gate()
	{
		if(open)
			return false;
		else
		{
			accessible = true;
			open = false;
			return true;
		} 
	}
	// Konstruktor pro branu
	gate(bool accessible, maze_cell_type cell_type, person * active_object):
	maze_cell(accessible, cell_type, active_object)
	{
		open = false;
	}
};

/**
 * Trida pro klic
 */
class key: public maze_cell
{
	private:
		bool took;

	public:
	// Pokud neni klic vzany vraci true
	virtual bool take_key()
	{
		if(!took)
		{
			took = true;
			return true;
		}
		else
			return false;
	}
	//pokud je klic vzany vraci cestu/hrace
	virtual maze_cell_type cell_type()
	{
		if(active_object != nullptr)
			return PERSON;
		else if(took)
			return PATH;
		else
			return KEY;
	}
	key(bool accessible, maze_cell_type cell_type, person * active_object):
	maze_cell(accessible, cell_type, active_object)
	{
		took = false;
	}
};

/**
 * Trida pro cil
 */
class finish: public maze_cell
{
	public:
		//pokud je cil ukazuje se cil/person
		virtual maze_cell_type cell_type()
		{
			if(active_object != nullptr)
				return PERSON;
			else
				return FINISH;
		}
		finish(bool accessible, maze_cell_type cell_type, person * active_object):
		maze_cell(accessible, cell_type, active_object){};
};



class game_plan
{
	private:
		int gameID;		// ID Hry

		unsigned int width; 		// Sirka herniho pole
		unsigned int height; 		// Vyska herniho pole
		maze_cell ***maze;		// Prvky herniho pole
		finish *end_game;

		std::vector<player*> players;
		std::vector<policeman*> police;

	public:
		game_plan(unsigned int, unsigned int, std::ifstream&);

	  maze_cell_type test(int x,int y)
   	{
   		return maze[x][y]->cell_type();
   	}

   	person *type_player(int x, int y)
   	{
   		return maze[x][y]->maze_cell::type_player();
   	}
};

class clientMessage
{
	public:
		int gameID;		// ID Hry
		int playerID;		// ID Hrace
		int message;		// ID Pozadavku
};

#endif
