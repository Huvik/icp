#ifndef gameLogic_H
#define gameLogic_H

#include <iostream>
#include <fstream>
#include <string>
#include <boost/asio.hpp>

#include "basicobj.h"

class gameLogic
{
	public:
		std::vector<gameplan*> *glGames;
		bool joinGame(int gameID);
		int createGame(gameInfo gi, mapsInfo mi, boost::asio::ip::udp::endpoint sender_endpoint, int ID);
		void playGame(int gameID);
		gameLogic();
		~gameLogic();
};

#endif
