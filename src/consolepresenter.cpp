/**
 * Projekt Bludiste do predmetu ICP 2013/2014
 * Authors: Jan Trcka - xtrcka07 (xtrcka07@stud.fit.vutbr.cz, trcka.j@gmail.com)
 *          Lukas Huvar - xhuvar00 (xhuvar00@stud.fit.vutbr.cz)
 * Date: 11. 05. 2014
 * Description: Trida majici na starosti zobrazovani dat ze serveru a komunikaci s uzivatelem
 **/


#include "consolepresenter.h"

/**
 * Nacte vstupni parametry a podle nich vytvori tridu pro komunikaci se serverem
 **/
consolePresenter::consolePresenter(std::string host, std::string port)
{
	cc = new clientCommunication(host, port);
}

/**
 * Uvolni pamet zabranou komunikacnim objektem
 **/
consolePresenter::~consolePresenter()
{
	delete cc;
}

/**
 * Prvotni start komunikace - Vypis hlavniho menu a podmenu
 **/
void consolePresenter::start()
{
	// Priprava potravnych ukazatelu a promennych
	gameInfo *games = NULL;
	mapsInfoSend *maps = NULL;
	int numOf = 0;
	std::string command = "nothing";
	std::string step;	

	// Ziskani ID client ze serveru
	cc->getID();

	// Smycka s hlavnim menu
	system("clear");
	do {
		// Podle zadaneho prikazu se rozhodneme co dal
		switch(checkCommand(command))
		{
			// Nic
			case 0:	break;
			// Nova hra
			case 1: 
			command = "Refresh";
			// Smycka s menu pro vytvoreni nove hry
			do {
				// Pokud nechce nacist mapy
				if(command.compare("Refresh"))
				{
					// Nacteme krok a pokusime se vytvorit novou hru
					std::cout << "Zadejte krok hry: ";
					std::cin >> step;
					if(!cc->createGame(command, step))
						// Hra se nevytvorila
						std::cout << "Spatne zadany prikaz, ID mapy, nebo krok" << std::endl;
					else {
						try {
							// Hra se vytvorila, rozpojuji vlakno na nacitani prikazu a vykreslovani hry
							std::thread t(&consolePresenter::listenPrint, this);
							readCommands();
							t.join();
						}
						// Odchytevani vyjimky
						catch (std::exception& e) 
						{
							std::cerr << e.what() << std::endl;
						}
						break;					
					}
				}
				// Jinak nacteme mapy
				else
				{
					cc->loadMaps(&maps, &numOf);
				}
				// Vypiseme mapy a nacteme prikaz
				printMaps(numOf, maps);
				std::cin >> command;
				system("clear");
			}while(command.compare("Back"));
			break;
			// Pripojeni k existujici hre
			case 2: 
			command = "Refresh";
			// Smycka pro pripojeni k jiz existujici hre
			do {
				if(command.compare("Refresh"))
				{
					// Pokus o pripojeni k jiz existujici hre
					if(!cc->joinGame(command))
						// Nevyslo to
						std::cout << "Pozadovana hra neexistuje, nebo je jiz plna" << std::endl;
					else {
						// Rozdeluji vlakno na nacitani prikazu a vykreslovani hry
						try {
							std::thread t(&consolePresenter::listenPrint, this);
							readCommands();
							t.join();
						}
						// Odchytavani vyjimek
						catch (std::exception& e) 
						{
							std::cerr << e.what() << std::endl;
						}

						break;
					}
				}
				else
				{
					// Nacteni vsech rozehranych her
					cc->loadGames(&games, &numOf);
				}
				// Vypis her a nacteni prikazu
				printGames(numOf, games);
				std::cin >> command;
				system("clear");				
			}while(command.compare("Back"));
			break;
			// Ukonceni clienta
			case 3: 
			delete cc;
			exit(0);
			break;
			// Spatne zadany prikaz
			default:
			std::cout << "Vas prikaz nebyl rozeznan" << std::endl << std::endl;

		}

		// Vypiseme menu a nacteme prikaz
		printMenu();
		std::cin >> command;
		system("clear");

	}while(true);

	delete cc;
	cc = NULL;
}

/**
 * Vypis Hlavniho menu
 **/
void consolePresenter::printMenu()
{
	using namespace std;
	cout << "--------------------------------------------------" << endl;
	cout << "----------------  Bludiste  2014  ----------------" << endl;
	cout << "--------------------------------------------------" << endl;
	cout << "Zadejte prikaz cinnosti, kterou chcete delat" << endl;
	cout << "POZOR! Prikazy jsou case sensitive!" << endl << endl;
	cout << "NewGame - Umozni vytvorit novou hru" << endl;
	cout << "JoinGame - Umozni pripojeni k jedne z jiz rozehranych her" << endl;
	cout << "Quit - Ukonci program" << endl << endl;
	cout << "Prikaz: ";
}

/**
 * Kontrola zadaneho prikazu
 * @param command Prikaz zadany uzivatelem
 * @return Hodnota odpovidajici prikazu
 **/
int consolePresenter::checkCommand(std::string command)
{
	if(!command.compare("nothing"))
		return 0;
	else if(!command.compare("NewGame"))
		return 1;
	else if(!command.compare("JoinGame"))
		return 2;
	else if(!command.compare("Quit"))
		return 3;
	return -1;
}

/**
 * Vypis Rozehranch her spolecne s menu
 * @param numOfGames Pocet rozehranych her
 * @param games Udaje o jednotlivych hrach
 **/
void consolePresenter::printGames(int numOfGames, gameInfo *games)
{
	using namespace std;
	cout << "--------------------------------------------------" << endl;
	cout << "----------------  Rozehrane  hry  ----------------" << endl;
	cout << "--------------------------------------------------" << endl;
	cout << "gameID --------- Num of players --------- Doba hry" << endl;
	cout << "--------------------------------------------------" << endl;

	// Pokud nejsou zadne hry
	if(numOfGames == 0)
		cout << "       Momentalne nejsou zadne rozehrane hry" << endl;

	// Jinak je vypiseme
	for(int i = 0; i < numOfGames; i++)
	{
		cout << std::to_string(games[i].gameID) << " --------- ";
		cout << std::to_string(games[i].numOfPlayers) << "/4 --------- ";
		cout << std::to_string(games[i].gameTime) << endl;
	}

	cout << "--------------------------------------------------" << endl;
	cout << "Zadejte prikaz cinnosti, kterou chcete delat" << endl;
	cout << "POZOR! Prikazy jsou case sensitive!" << endl << endl;
	cout << "<gameID> - Umozni pripojeni k jiz rozehrane hre" << endl;
	cout << "Refresh - Znovu nacte rozehrane hry" << endl;
	cout << "Back - Vrati do hlavniho menu" << endl << endl;
	cout << "Prikaz: ";	
}

/**
 * Vypis hratelnych map a jejich informaci
 * @param numOfMaps Pocet hratelnych map
 * @param maps Udaje o jednotlivych mapach
 **/
void consolePresenter::printMaps(int numOfMaps, mapsInfoSend *maps)
{
	using namespace std;
	cout << "--------------------------------------------------" << endl;
	cout << "-------------------  Nová hra  -------------------" << endl;
	cout << "--------------------------------------------------" << endl;
	cout << "bludisteID --------- Sirka --------- Vyska" << endl;
	cout << "--------------------------------------------------" << endl;

	// Pokud nejsou zadne mapy
	if(numOfMaps == 0)
		cout << "     Momentalne nejsou k dispozici zadne mapy" << endl;

	// Jinak vypiseme jejich udaje
	for(int i = 0; i < numOfMaps; i++)
	{
		cout << i << " --------- ";
		cout << std::to_string(maps[i].width) << " --------- ";
		cout << std::to_string(maps[i].height) <<  endl;
	}

	cout << "--------------------------------------------------" << endl;
	cout << "Zadejte prikaz cinnosti, kterou chcete delat" << endl;
	cout << "POZOR! Prikazy jsou case sensitive!" << endl << endl;
	cout << "<bludisteID> - Zalozi novou hru s mapou tohoto bludiste" << endl;
	cout << "Refresh - Znovu vsechny mapy" << endl;
	cout << "Back - Vrati do hlavniho menu" << endl << endl;
	cout << "Prikaz: ";
}

/**
 * Funkce pro vypis hraci plochy hry. Pocka dokud nedostane hru od serveru a pak ji vypise.
 **/
void consolePresenter::listenPrint()
{
	// Hraci plocha
	gameplan *gp = new gameplan();
	do{
		// Nacteni hraci plochy
		cc->listenGame(gp);

		// Pokud doslo k chybe, ukoncime hru
		if(gp->msg == Error)
		{
			std::cerr << "Ouha, neco se pokazilo na serveru, prosim zkuste to pozdeji" << std::endl;
			break;
		}

		// Jinak vypiseme hraci plochu
		using namespace std;
		system("clear");
		for(int y = 0; y < gp->height; y++)
		{
			for(int x = 0; x < gp->width; x++)
			{
				std::cout << gp->maze[gp->width*y+x];
			}
			cout << endl;
		}
		cout << "<^v> pro hlidace ^ - nahoru v - dolu < - doprava > - doleva" << endl
 		<< "awsd pro cerveneho hrace" << endl
		<< "ftgh pro modreho hrace" << endl
 		<< "jikl pro zeneleho hrace" << endl
 		<< "4856 pro zluteho hrace" <<endl;
	}while(true);
}

/**
 * Cte prikazy uzivatele a kontroluje je. Pokud jsou spravne zadany, odeslou se k provedeni
 **/
void consolePresenter::readCommands()
{
	std::string command;
	do
	{
		std::cin >> command;
		cc->sendCommand(command);
	}while(command.compare("leave"));
}
