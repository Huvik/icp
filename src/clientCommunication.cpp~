#include "clientCommunication.h"

clientCommunication::clientCommunication()
{
	connect("localhost", "5000");
}

clientCommunication::clientCommunication(std::string ip, std::string port)
{
	connect(ip, port);
}

clientCommunication::~clientCommunication()
{
	delete socket;
	delete receiver_endpoint;
}

void clientCommunication::connect(std::string ip, std::string port)
{
	try 
	{
		key_t key = ftok("icpCC", 1);
		sem_id = semget(key, 1, IPC_CREAT | IPC_EXCL | 0666);

		semSet();

		gameID = 0;

		// Zpristupneni udp
		using boost::asio::ip::udp;
	
		// Vytvoreni spojeni
	        boost::asio::io_service io_service;
	
	        udp::resolver resolver(io_service);
	        udp::resolver::query query(udp::v4(), ip, port);
	        receiver_endpoint = new udp::endpoint(*resolver.resolve(query));
	
	        socket = new udp::socket(io_service);
	        socket->open(udp::v4());

	} 
	catch (std::exception& e) 
	{
	        std::cerr << e.what() << std::endl;
	}
}

void clientCommunication::getID()
{
	try 
	{
		// Nastaveni prvotni zpravy
		clientMessage clm;
		clm.message = NewPlayer;

		socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);

		// Nacteni odpovedi a ulozeni ID hrace
		socket->receive_from(boost::asio::buffer(&playerID, sizeof(int)), *receiver_endpoint);
		std::cout << std::to_string(playerID) << std::endl;
	} 
	catch (std::exception& e) 
	{
	        std::cerr << e.what() << std::endl;
	}
}

bool clientCommunication::joinGame(std::string gameID)
{
	try 
	{
		// Nastaveni prvotni zpravy
		clientMessage clm;
		clm.message = JoinGame;
		clm.gameID = std::strtol(gameID);

		socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);	

		socket->receive_from(boost::asio::buffer(&(this->gameID), sizeof(int)), *receiver_endpoint);

		if(this->gameID == 0)
			return false;
		return true;
	} 
	catch (std::exception& e) 
	{
	        std::cerr << e.what() << std::endl;
		return false;
	}
}

void clientCommunication::loadGames(gameInfo **games, int *numOfGames)
{
	try 
	{
		delete *games;
		*games = nullptr;

		// Nastaveni prvotni zpravy
		clientMessage clm;
		clm.message = SendGames;

		socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);

		// Nacteni odpovedi a ulozeni ID hrace
		socket->receive_from(boost::asio::buffer(numOfGames, sizeof(int)), *receiver_endpoint);
		
		if(*numOfGames == 0)
			return;

		*games = new gameInfo[*numOfGames];

		socket->receive_from(boost::asio::buffer(*games, sizeof(gameInfo) * (*numOfGames)), *receiver_endpoint);
	} 
	catch (std::exception& e) 
	{
	        std::cerr << e.what() << std::endl;
	}
}

void clientCommunication::loadMaps(mapsInfoSend **maps, int *numOfMaps)
{
	try 
	{
		delete *maps;
		*maps = nullptr;

		// Nastaveni prvotni zpravy
		clientMessage clm;
		clm.message = SendMaps;

		socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);

		// Nacteni odpovedi a ulozeni ID hrace
		socket->receive_from(boost::asio::buffer(numOfMaps, sizeof(int)), *receiver_endpoint);
		
		if(*numOfMaps == 0)
			return;

		*maps = new mapsInfoSend[*numOfMaps];

		socket->receive_from(boost::asio::buffer(*maps, sizeof(mapsInfoSend) * (*numOfMaps)), *receiver_endpoint);
	} 
	catch (std::exception& e) 
	{
	        std::cerr << e.what() << std::endl;
	}
}

bool clientCommunication::createGame(std::string mapID)
{
	try 
	{
		// Nastaveni prvotni zpravy
		clientMessage clm;
		clm.message = CreateGame;
		clm.gameID = std::strtol(mapID);

		socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);	

		socket->receive_from(boost::asio::buffer(&gameID, sizeof(int)), *receiver_endpoint);

		if(gameID == 0)
			return false;
		return true;
	} 
	catch (std::exception& e) 
	{
	        std::cerr << e.what() << std::endl;
		return false;
	}
}

void clientCommunication::listenGame(gameplan *gp)
{
	semop(sem_id, &semWait, 0);
	socket->receive_from(boost::asio::buffer(gp, sizeof(gameplan)), *receiver_endpoint);

	if(gp->msg != Error && gameID == 0)
		gameID = gp->gameID;
	semop(sem_id, &semSignal, 0);
}

void clientCommunication::sendCommand(std::string command)
{
	// Nastaveni prvotni zpravy
	clientMessage clm;
	clm.message;
	clm.playerID = this->playerID;
	clm.gameID = this->gameID;
	
	if(!command.compare("go"))
		clm.message = Go;
	else if(!command.compare("stop"))
		clm.message = Stop;
	else if(!command.compare("left"))
		clm.message = Left;
	else if(!command.compare("right"))
		clm.message = Right;
	else if(!command.compare("take"))
		clm.message = Take;
	else if(!command.compare("open"))
		clm.message = Open;
	else
		return;
	semop(sem_id, &semWait, 0);

	socket->send_to(boost::asio::buffer(&clm, sizeof(clientMessage)), *receiver_endpoint);
	semop(sem_id, &semSignal, 0);
}

/**
 * Funkce, ktera nastavi menici signaly semaforu.
 * @param var Informacni struktura se vsemi dulezetymi informacemi
 * @param semNum Cislo udavajici pocet semaforu
 **/
void clientCommunication::semSet()
{
	semctl(sem_id, 0, SETVAL, 0);

	semWait.sem_num = 0;
	semWait.sem_op = -1;
	semWait.sem_flg = 0;

	semSignal.sem_num = 0;
	semSignal.sem_op = 1;
	semSignal.sem_flg = 0;
}
