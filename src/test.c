#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <ctime>
#include <random>
#include <cmath>
#include <chrono>
#include <thread> 

// Typy prvku bludiste
enum mazeCellType {
		WALL,		// Zed
		PATH,		// Cesta
		GATE_CLOSED,	// Uzavrena brana
		FINISH,		// Cil
		KEY,		// Klic
		PERSON 	//Hrac Policeman 
	};

// Orientace pohyblivych prvku
	enum orientation { UP, DOWN, LEFT, RIGHT};

// Zpravy, ktere muze poslat server a on se na zaklade toho rozhodne co delat
	enum clientMsg {NewPlayer,
		SendGames,
		SendMaps,
		CreateGame,
		JoinGame,
		Error,
		Game,
		Step,
		Go,
		Stop,
		Left,
		Right,
		Take,
		Open,
		Keys};

		enum move_result {EMPTY, OCCUPIED, DEAD};

		enum players{RED, BLUE, GREEN, YELLOW, POLICEMAN};

class person;		

/**
 * Trida pro herni prvky planu rodic 
 * Cesta nebo Zed jinak 
 */
 class maze_cell
 {
 protected:
		person *active_object;	//aktivni prvek na policku 
		mazeCellType type;	// Typ pole (jiz primo o co se jedna)
		bool accessible;	// Prostupnost pole
	public:

		/**
		 * Vraci hodnotu policka pro cestu je volna
		 */
		 virtual move_result empty();

		/**
		 * Vraci hodnotu true pokud jde vzit klic jinak false
		 */
		 virtual bool take_key()
		 {
		 	return false;
		 }
		/**
		 * Vraci hodnotu true pokud jde brana otevrit jinak false
		 */
		 virtual bool open_gate()
		 {
		 	return false;
		 }

		/**
		 * Bunka nebo na ni stoji hrac
		 */
		 virtual mazeCellType cell_type()
		 {
		 	if(active_object == nullptr)
		 		return type;
		 	else
		 		return PERSON;
		 }

		/*
		 * Vraci adresu hrace pro vykresleni
		 */
		 person *type_player()
		 {
		 	return active_object;
		 }

		/*
		 * Zmeni adresu hrace na zvolenou
		 */
		 void changeAddr(person *p)
		 {
		 	active_object = p;
		 }
		//Konstruktor
		 maze_cell(bool, mazeCellType, person*);
		//Destruktor
		 ~maze_cell()
		 {
		 	active_object = nullptr;
		 }
		};

/**
 * Trida pro branu
 */
 class gate: public maze_cell
 {
 private:
 	bool open;
 public:
	// Pokud je brana zavrena nejde vstoupit
 	virtual move_result empty()
 	{
 		if (open)
 			return EMPTY;
 		else
 			return OCCUPIED;
 	}
	// Pokud je brana zavrena tvari se jako brana jinak jako volne pole
 	virtual mazeCellType cell_type()
 	{
 		if(active_object != nullptr)
 			return PERSON;
 		else if(open)
 			return PATH;
 		else
 			return GATE_CLOSED;
 	}
	// Otevreni brany
 	virtual bool open_gate()
 	{
 		if(open)
 			return false;
 		else
 		{
 			accessible = true;
 			open = false;
 			return true;
 		} 
 	}
	// Konstruktor pro branu
 	gate(bool accessible, mazeCellType cell_type, person * active_object):
 	maze_cell(accessible, cell_type, active_object)
 	{
 		open = false;
 	}
 };

/**
 * Trida pro klic
 */
 class key: public maze_cell
 {
 private:
 	bool took;

 public:
	// Pokud neni klic vzany vraci true
 	virtual bool take_key()
 	{
 		if(!took)
 		{
 			took = true;
 			return true;
 		}
 		else
 			return false;
 	}
	//pokud je klic vzany vraci cestu/hrace
 	virtual mazeCellType cell_type()
 	{
 		if(active_object != nullptr)
 			return PERSON;
 		else if(took)
 			return PATH;
 		else
 			return KEY;
 	}
 	key(bool accessible, mazeCellType cell_type, person * active_object):
 	maze_cell(accessible, cell_type, active_object)
 	{
 		took = false;
 	}
 };

/**
 * Trida pro cil
 */
 class finish: public maze_cell
 {
 public:
		//pokud je cil ukazuje se cil/person
 	virtual mazeCellType cell_type()
 	{
 		if(active_object != nullptr)
 			return PERSON;
 		else
 			return FINISH;
 	}
 	finish(bool accessible, mazeCellType cell_type, person * active_object):
 	maze_cell(accessible, cell_type, active_object){};
 };


		/** 
		 * Trida pro policii
		 *
		 */
		class person
		{
			protected:
				unsigned int x;
				unsigned int y;
				orientation faceOrientation;
				players typePlayer;
				maze_cell ***mapa;

			public:
				person(maze_cell*** mapa, unsigned int x, unsigned int y);
				orientation returnOrientation()
				{
					return faceOrientation;
				}

				players returnPlayer()
				{
					return typePlayer;
				}
				virtual bool step();
				virtual bool move();
		};

		class player: public person
		{
			private:
				bool alive;
				unsigned int steps;
				unsigned int keys;			
			public:

				void kill()
				{
					alive = false;
				}


		};

		bool person::step()
		{
			int xInc = 0, yInc = 0;
			//Zjisteni strany otoceni
			switch (faceOrientation)
			{
				case UP:
					yInc = -1;
					break;
				case DOWN:
					yInc = 1;
					break;
				case LEFT:
					xInc = -1;
					break;
				case RIGHT:
					xInc = 1;
					break;
			}

			//Posun podle natoceni policajta
			switch(mapa[y+yInc][x+xInc]->empty())
			{
				//Posun na volne policko
				case EMPTY:
					mapa[y+yInc][x+xInc]->changeAddr(this);
					mapa[y][x]->changeAddr(nullptr);
					x+=xInc;
					y+=yInc;
					break;
				//Posun + zabiti hrace na policku
				case DEAD:
					mapa[y+yInc][x+xInc]->type_player()->kill();
					mapa[y+yInc][x+xInc]->changeAddr(this);
					mapa[y][x]->changeAddr(nullptr);
					x+=xInc;
					y+=yInc;
					break;	
			}
		}

		bool person::move()
		{
			std::random_device random;
			std::default_random_engine engine(random());
			std::uniform_int_distribution<int> uniform_dist(0, 8);
			int move = uniform_dist(engine);

			if(move > 3)
				step();
			else
				faceOrientation = static_cast<orientation>(move);
		}

		person::person(maze_cell*** mapa, unsigned int x, unsigned int y)
		{
			this->x = x;
			this->y = y;
			std::random_device random;
  		std::default_random_engine engine(random());
  		std::uniform_int_distribution<int> uniform_dist(0, 3);
  		this->faceOrientation = static_cast<orientation>(uniform_dist(engine));	
			this->typePlayer = POLICEMAN;
			this->mapa = mapa;
		}




 class game
 {
 private:
 	unsigned int width, height;
 	maze_cell ***maze;
 	std::vector<person*> police;
 public:
 	game(unsigned int, unsigned int, std::ifstream&);
 	void print();
 	void movePolice();
 };


void game::movePolice()
{
	for(auto x: police)
    x->move();
}


 void game::print()
 {
 	for (int y = 0; y < height; ++y)
 	{
 		for (int x = 0; x < width; ++x)
 		{
 			if(maze[y][x]->cell_type() == WALL)
 				std::cout << "#";
 			if(maze[y][x]->cell_type() == PATH)
 				std::cout << ".";
 			if(maze[y][x]->cell_type() == FINISH)
 				std::cout << "F";
 			if(maze[y][x]->cell_type() == GATE_CLOSED)
 				std::cout << "G";
 			if(maze[y][x]->cell_type() == KEY)
 				std::cout << "K";
 			if(maze[y][x]->cell_type() == PERSON)
 			{
 				switch(maze[y][x]->type_player()->returnOrientation())
 				{
 					case UP:
 						std::cout << "^";
 						break;
 					case DOWN:
 						std::cout << "v";
 						break;
 					case RIGHT:
 						std::cout << ">";
 						break;
 					case LEFT:
 						std::cout << "<";
 						break;	
 				}
 			}		
 		}
 		std::cout << std::endl;
 	}
 }
 game::game(unsigned int height, unsigned int width, std::ifstream &map)
 {
 	this->width = width;
 	this->height = height;

 	char c;

	//Create array of pointers 
 	maze = new maze_cell**[height];
 	if(maze == nullptr)
 		exit(1);

	//2D array of pointers
 	for (int i = 0; i < height; ++i)
 	{
 		maze[i] = new maze_cell* [width];
 		if(maze[i] == nullptr)
 			exit(1);
 	}

 	for (int y = 0; y < height; ++y)
 	{
 		for (int x = 0; x < width; ++x)
 		{
 			map >> c;
 			if(c == '#')
 				maze[y][x] = new maze_cell(false, WALL, nullptr);
 			if(c == '.')
 				maze[y][x] = new maze_cell(true, PATH, nullptr);
 			if(c == 'F')
 			{
 				maze[y][x] = new finish(true, FINISH, nullptr);;
 			}
 			if(c == 'K')
 				maze[y][x] = new key(true, KEY, nullptr);
 			if(c == 'G')
 				maze[y][x] = new gate(false, GATE_CLOSED, nullptr);
 			if(c == 'P')
 			{
 				person *n = new person(maze, x, y);
 				maze[y][x] = new maze_cell(true, PATH, n);
 				police.push_back(n);
 			}
 		}
 	}
 }


/* Vraci EMPTY, pokud je policko prazdne
 * Vraci OCCUPIED, pokud je na policku hrac nebo zavrena brana/zed
 * Vraci DEAD, pokud hrac chce vejit na policko
 */
 move_result maze_cell::empty()
 {
 	if(active_object == nullptr)
 	{
 		if(accessible)
 			return EMPTY;
 		else
 			return OCCUPIED;
 	}
 	else  
 	{
 		if(active_object->returnPlayer() == POLICEMAN)
 			return DEAD;
 		else
 			return OCCUPIED;
 	}
 }




// Konstruktor pro prvek pole
 maze_cell::maze_cell (bool accessible, mazeCellType type, person *active_object)
 {
 	this->accessible = accessible;
 	this->type = type;
 	this->active_object = active_object;
 }

 const int max_size = 50;
 const int min_size = 20;



 int main(int argc, char const *argv[])
 {
 	std::ifstream game_map;
	//open map 
 	game_map.open("../examples/20x30");
 	if(!game_map)
 	{
 		std::cerr << "Cant open game_map" << std::endl;
 		return -1;
 	}
 	std::string line;
	//read first line with size of map
 	std::getline(game_map, line);
 	int Row,Column;
 	char x;
	//parse first line to size of map
 	std::istringstream iss(line);
 	iss >> Row >> x >> Column;
 	if( Row > max_size || Row < min_size || Column > max_size || Column < min_size)
 	{
 		std::cerr << "Bad size of map" << std::endl;
 		return -1;
 	}

 	game new_game(Row,Column, game_map);

 	new_game.print();

 	while(1)
 	{
 		system("clear");
 		new_game.movePolice();
 		new_game.print();
 		std::this_thread::sleep_for (std::chrono::seconds(1));
 	}



 	return 0;
 }
