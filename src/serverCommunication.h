#ifndef serverCommunication_H
#define serverCommunication_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <boost/asio.hpp>
#include <thread> 
#include <dirent.h>
#include <sys/types.h>
#include <chrono>

#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>

#include "basicObj.h"
#include "gameLogic.h"

class serverCommunication
{
	private:
		int sem_id;
		struct sembuf semWait;
		struct sembuf semSignal;
	public:

		gameLogic *gl;
		int pomPlayerID;
		int pomGameID;
		std::vector<gameInfo>* games;
		std::vector<mapsInfo>* maps;
		serverCommunication();
		void loadMaps();
		void newThr(boost::asio::ip::udp::endpoint sender_endpoint, boost::asio::ip::udp::socket *sock, clientMessage clm);
		bool sendGame(boost::asio::ip::udp::socket *sock, int gameID);
		void semSet();
		player* getPlayer(clientMessage msg);
};

#endif
