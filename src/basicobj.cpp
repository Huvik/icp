/**
 * Projekt Bludiste do predmetu ICP 2013/2014
 * Authors: Jan Trcka - xtrcka07 (xtrcka07@stud.fit.vutbr.cz, trcka.j@gmail.com)
 *          Lukas Huvar - xhuvar00 (xhuvar00@stud.fit.vutbr.cz)
 * Date: 11. 05. 2014
 * Description: Prace s objekty pole
 **/

#include "basicobj.h"


gameplan::gameplan(gameInfo gi, mapsInfo mi)
{
	this->gameID = gi.gameID;
	this->numOfPlayers = gi.numOfPlayers;
	this->gameTime = gi.gameTime;
	


	this->width = mi.width;
	this->height = mi.height;
	this->mazeID = mi.mazeID;
	this->mapName = mi.mapName;
}


/**
 * Nakopiruje hraci plochu do pole charu pro jednodusi posilani
 * # - zed, . - cesta, K - klic, F - cil, G - zavrena brana,
 * <^v> pro hlidace ^ - nahoru v - dolu < - doprava > - doleva
 * awsd pro cerveneho hrace
 * ftgh pro modreho hrace
 * jikl pro zeneleho hrace
 * 4856 pro zluteho hrace
 */
void game::createMap(char *buffer)
{	
	int i = 0;
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			switch(maze[y][x]->cell_type())
			{
				case PATH:
					buffer[i] = '.';
					break;
				case WALL:
					buffer[i] = '#';
					break;
				case FINISH:
					buffer[i] = 'F';
					break;
				case GATE_CLOSED:
					buffer[i] = 'G';
					break;
				case KEY:
					buffer[i] = 'K';
					break;
				case PERSON:
					switch(maze[y][x]->type_player()->returnPlayer())
					{
						case POLICEMAN:
							switch(maze[y][x]->type_player()->returnOrientation())
							{
								case UP:
									buffer[i] = '^';
									break;
								case DOWN:
									buffer[i] = 'V';
									break;
								case RIGHT:
									buffer[i] = '>';
									break;
								case LEFT:
									buffer[i] = '<';
									break;
							}
							break;
						case RED:
							switch(maze[y][x]->type_player()->returnOrientation())
							{
								case UP:
									buffer[i] = 'w';
									break;
								case DOWN:
									buffer[i] = 's';
									break;
								case RIGHT:
									buffer[i] = 'd';
									break;
								case LEFT:
									buffer[i] = 'a';
									break;
							}
							break;
						case BLUE:
							switch(maze[y][x]->type_player()->returnOrientation())
							{
								case UP:
									buffer[i] = 't';
									break;
								case DOWN:
									buffer[i] = 'g';
									break;
								case RIGHT:
									buffer[i] = 'h';
									break;
								case LEFT:
									buffer[i] = 'f';
									break;
							}
							break;
						case GREEN:
							switch(maze[y][x]->type_player()->returnOrientation())
							{
								case UP:
									buffer[i] = 'i';
									break;
								case DOWN:
									buffer[i] = 'k';
									break;
								case RIGHT:
									buffer[i] = 'l';
									break;
								case LEFT:
									buffer[i] = 'j';
									break;
							}
							break;
						case YELLOW:
							switch(maze[y][x]->type_player()->returnOrientation())
							{
								case UP:
									buffer[i] = '8';
									break;
								case DOWN:
									buffer[i] = '5';
									break;
								case RIGHT:
									buffer[i] = '4';
									break;
								case LEFT:
									buffer[i] = '6';
									break;
							}
							break;	

					}	
					break;
			}
			i++;
		}
	}
}

/**
* Virtualni metoda pro soubezny pohyb
* @return  TRUE/FALSE pokud je mozne udelat krok nebo ne
*/
bool person::move()
{
	std::random_device random;
	std::default_random_engine engine(random());
	std::uniform_int_distribution<int> uniform_dist(0, 8);
	int move = uniform_dist(engine);
	//Rozhodovani jestli policie udela krok nebo se otoci
	if(move > 3)
		step();
	else
		faceOrientation = static_cast<orientation>(move);

	return true;
}

//Konstruktor pro policii
person::person(maze_cell*** mapa, unsigned int x, unsigned int y)
{
	this->x = x;
	this->y = y;
	std::random_device random;
	std::default_random_engine engine(random());
	std::uniform_int_distribution<int> uniform_dist(0, 3);
	this->faceOrientation = static_cast<orientation>(uniform_dist(engine));	
	this->typePlayer = POLICEMAN;
	this->mapa = mapa;
}

/**
* Virtualni metoda pro krok
* @return  TRUE/FALSE pokud je mozne udelat krok nebo ne
*/	
bool person::step()
{
	int xInc = 0, yInc = 0;
	//Zjisteni strany otoceni
	switch (faceOrientation)
	{
		case UP:
			yInc = -1;
			break;
		case DOWN:
			yInc = 1;
			break;
		case LEFT:
			xInc = -1;
			break;
		case RIGHT:
			xInc = 1;
			break;
	}
	//Posun podle natoceni policajta
	switch(mapa[y+yInc][x+xInc]->empty())
	{
		//Posun na volne policko
		case EMPTY:
			mapa[y+yInc][x+xInc]->changeAddr(this);
			mapa[y][x]->changeAddr(nullptr);
			x+=xInc;
			y+=yInc;
			break;
		//Posun + zabiti hrace na policku
		case OCCUPIED:
			mapa[y+yInc][x+xInc]->type_player()->kill();
			mapa[y+yInc][x+xInc]->changeAddr(this);
			mapa[y][x]->changeAddr(nullptr);
			x+=xInc;
			y+=yInc;
			break;
		case TAKEN:
		case DEAD:
			break;	
	}

	return true;
}


/* Vraci EMPTY, pokud je policko prazdne
 * Vraci OCCUPIED, pokud je na policku hrac nebo zavrena brana/zed
 * Vraci DEAD, pokud hrac chce vejit na policko
 */
move_result maze_cell::empty()
{
	if(active_object == nullptr)
	{
		if(accessible)
			return EMPTY;
		else
			return TAKEN;
	}
	else  
	{
		if(active_object->returnPlayer() == POLICEMAN)
			return DEAD;
		else
			return OCCUPIED;
	}
}




// Konstruktor pro prvek pole
maze_cell::maze_cell (bool accessible, mazeCellType type, person *active_object)
{
	this->accessible = accessible;
	this->type = type;
	this->active_object = active_object;
}



/**
 * Umisteni hrace na hraci plochu a pripojeni do hry
 */
void player::joinGame(int ID, players p, unsigned int xSize, unsigned int ySize, maze_cell ***m)
{
	unsigned int xInc = 1, yInc = 1, x = 1, y = 1;
	mapa = m;
	//nastaveni prohladavani souradnic pro hrace
	//cerveny levy vrchni roh
	//modry pravy vrchni roh
	//zeleny levy dolni roh
	//zluty pravy dolno roh
	switch(p)
	{
		case RED:
			break;
		case BLUE:
			xInc = -1;
			x = xSize;
			xSize = 1;
			break;
		case GREEN:
			yInc = -1;
			y = ySize;
			ySize = 1;
			break;
		case YELLOW:
			yInc = -1;
			xInc = -1;
			y = ySize;
			ySize = 1;
			x = xSize;
			xSize = 1;
			break;
	}

	bool dale = true;

	for (; y != ySize; y+=yInc)
	{
		for (; x != xSize; x+=xInc)
		{
			if(m[y][x]->empty() == EMPTY)
			{
				dale = false;
				break;
			}			
		}
		if(!dale)
			break;
	}


	playerID = ID;
	typePlayer = p;
	this->x = x;
	this->y = y;
	mapa[y][x]->changeAddr(this);
	alive = true;
}




/**
 * Pokus o otevreni brany
 * @return true/false pokud brana byla otevrena
 */
bool player::openGate()
{
	int posunY = 0, posunX = 0;
	switch(faceOrientation)
	{
		case UP:
			posunY = -1;
			break;
		case DOWN:
			posunY = 1;
			break;
		case LEFT:
			posunX = -1;
			break;
		case RIGHT:
			posunX = 1;
			break;
	}
	if(mapa[y+posunY][x+posunX]->open_gate())
	{
		keys--;
		return true;
	}
	else
		return false;
}

/**
 * Pokus o sebrani klice ze zeme  
 * @return true/false pokud jde vzit klic
 */
bool player::pickKey()
{
	int posunY = 0, posunX = 0;
	switch(faceOrientation)
	{
		case UP:
			posunY = -1;
			break;
		case DOWN:
			posunY = 1;
			break;
		case LEFT:
			posunX = -1;
			break;
		case RIGHT:
			posunX = 1;
			break;
	}
	if(mapa[y+posunY][x+posunX]->take_key())
	{
		keys++;
		return true;
	}
	else
		return false;
}


/** 
 * Vyhodnoti jestli se hrac muze posunout a vraci true/false
 * nastavuje vysledne hodnoty policka
 * @return  vraci true/false pokud muze jit nebo ne
 */
bool player::step()
{
	maze_cell *me = mapa[y][x];
	maze_cell *pom = nullptr;
	int posunX = 0, posunY = 0;
	//vypocet posunu
	switch(faceOrientation)
	{
		case UP:
			posunY = -1;
			break;
		case DOWN:
			posunY = 1;
			break;
		case LEFT:
			posunX = -1;
			break;
		case RIGHT:
			posunX = 1;
			break;
	}
	//ziskani ciloveho policka
	pom = mapa[y+posunY][x+posunX];

	//nastaveni hodnot po posunu
	switch(pom->empty())
	{
		case EMPTY:
			pom->changeAddr(me->type_player());
			me->changeAddr(nullptr);
			steps++;
			x+=posunX;
			y+=posunY;
			return true;
		case TAKEN:
		case OCCUPIED:
			return false;
		case DEAD:
			me->type_player()->kill();
			me->changeAddr(nullptr);
			return true;
	}
}
/**
 * Vraci uslych kroku
 * @return  pocet uslych kroku
 */
int player::retSteps()
{
	return steps;
}
/**
 * Otoci hrace doprava
 */
void player::right()
{
	switch(faceOrientation)
	{
		case UP:
			faceOrientation = RIGHT;	
			break;
		case LEFT:
			faceOrientation = UP;	
			break;
		case DOWN:
			faceOrientation = LEFT;	
			break;
		case RIGHT:
			faceOrientation = DOWN;	
			break;
	}
}
/**
 * Otoci hrace doleva
 */
void player::left()
{
	switch(faceOrientation)
	{
		case UP:
			faceOrientation = LEFT;	
			break;
		case LEFT:
			faceOrientation = DOWN;	
			break;
		case DOWN:
			faceOrientation = RIGHT;	
			break;
		case RIGHT:
			faceOrientation = UP;	
			break;
	}
}

 /**
  * Konstruktor pro hru
  */
 game::game(unsigned int height, unsigned int width, std::ifstream &map, char *buff)
 {
 	this->width = width;
 	this->height = height;
 	buffer = buff;
 	char c;

	//Create array of pointers 
 	maze = new maze_cell**[height];
 	if(maze == nullptr)
 		exit(1);

	//2D array of pointers
 	for (int i = 0; i < height; ++i)
 	{
 		maze[i] = new maze_cell* [width];
 		if(maze[i] == nullptr)
 			exit(1);
 	}
 	for (int y = 0; y < height; ++y)
 	{
 		for (int x = 0; x < width; ++x)
 		{
 			map >> c;
 			if(c == '#')
 				maze[y][x] = new maze_cell(false, WALL, nullptr);
 			if(c == '.')
 				maze[y][x] = new maze_cell(true, PATH, nullptr);
 			if(c == 'F')
 			{
 				maze[y][x] = new finish(true, FINISH, nullptr);;
 			}
 			if(c == 'K')
 				maze[y][x] = new key(true, KEY, nullptr);
 			if(c == 'G')
 				maze[y][x] = new gate(false, GATE_CLOSED, nullptr);
 			if(c == 'P')
 			{
 				person *n = new person(maze, x, y);
 				maze[y][x] = new maze_cell(true, PATH, n);
 				police.push_back(n);
 			}
 		}
 	}
 }

/**
 * Pri kazdem kole pohybuje s kazdym policajtem po mape
 */
void game::movePolice()
{
	for(auto x: police)
		x->move();
}
