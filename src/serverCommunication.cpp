#include "serverCommunication.h"

void serverCommunication::newThr(boost::asio::ip::udp::endpoint sender_endpoint, boost::asio::ip::udp::socket *sock, clientMessage clm)
{

	semop(sem_id, &semWait, 0);

	mapsInfo mi;
	mi = maps->at(clm.gameID);

	gameInfo gi;
	gi.gameID = pomGameID++;
	gi.numOfPlayers = 1;
	gi.gameTime = time(0);	

	games->push_back(gi);

	int gameID = gl->createGame(gi, mi, sender_endpoint);

	semop(sem_id, &semSignal, 0);

	if(gameID == 0)
		return;

	while(true)
	{

		std::this_thread::sleep_for(std::chrono::milliseconds(2000));

		semop(sem_id, &semWait, 0);

		gl->playGame(gameID);

		if(!sendGame(sock, gameID))
			break;
		semop(sem_id, &semSignal, 0);
	}

	for(int i = 0; i < gl->glGames->size(); i++) {
		if((gl->glGames->at(i)).gameID == gameID) {
			gl->glGames->erase(gl->glGames->begin()+i);
		}
	}

	for(int i = 0; i < games->size(); i++) {
		if((games->at(i)).gameID == gameID) {
			this->games->erase(this->games->begin()+i);
		}
	}	

	semop(sem_id, &semSignal, 0);
	//sock.send_to(boost::asio::buffer(mi, sizeof(mapsInfo) * pom), sender_endpoint);
}

serverCommunication::serverCommunication()
{
	key_t key = ftok("icp", 1);
	sem_id = semget(key, 1, IPC_CREAT | IPC_EXCL | 0666);

	semSet();
	pomPlayerID = 1;
	pomGameID = 1;
	gl = new gameLogic();
	games = new std::vector<gameInfo>();
	maps = new std::vector<mapsInfo>();

	loadMaps();

	std::thread *connectionThread;

	using boost::asio::ip::udp;
	try {
		boost::asio::io_service io_service;

		udp::socket sock(io_service, udp::endpoint(udp::v4(), 5000));

		clientMessage clm;
		udp::endpoint sender_endpoint;
		while (true) {

			semop(sem_id, &semWait, 0);

			size_t length = sock.receive_from(boost::asio::buffer(&clm, sizeof(clientMessage)), sender_endpoint);
			int pom;

std::cout << "Prijato: " << std::to_string(clm.message) << std::endl;

			switch(clm.message)
			{
				case NewPlayer: 
						pom = pomPlayerID++;
						sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
					break;
				case SendGames:

						pom = games->size();
						sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);

						if(pom != 0) 
						{
							gameInfo *g = new gameInfo[pom];
							for(int i = 0; i < pom; i++)
							{
								g[i] = games->at(i);
								g[i].gameTime = time(0) - g[i].gameTime;
							}						

							sock.send_to(boost::asio::buffer(g, sizeof(gameInfo) * pom), sender_endpoint);
							delete g;
						}
				

					break;
				case SendMaps:						
						pom = maps->size();
						sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);

						if(pom != 0) 
						{
							mapsInfoSend *mi = new mapsInfoSend[pom];

							for(int i = 0; i < pom; i++){
								mi[i].width = maps->at(i).width;
								mi[i].height = maps->at(i).height;
								mi[i].mazeID = maps->at(i).mazeID;
							}		

							sock.send_to(boost::asio::buffer(mi, sizeof(mapsInfoSend) * pom), sender_endpoint);
							//delete mi;
						}
					break;
				case CreateGame:
						pom = false;
						if(clm.gameID < maps->size()) {
							pom = true;
							sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
							connectionThread = new std::thread(&serverCommunication::newThr, this, sender_endpoint, &sock, clm);
						}
						else
							sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
					break;
				case JoinGame:
						pom = false;
						for(int i = 0; i < gl->glGames->size(); i++) {
							if(clm.gameID == gl->glGames->at(i).gameID)	{
								if(gl->glGames->at(i).numOfPlayers < 4)
								{
									gl->glGames->at(i).players[gl->glGames->at(i).numOfPlayers].sender_endpoint = sender_endpoint;
									gl->glGames->at(i).numOfPlayers++;
									for(int a = 0; a < games->size(); a++) {
										if(clm.gameID == games->at(i).gameID)
											games->at(i).numOfPlayers++;
									}
									pom = gl->glGames->at(i).gameID;
									sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
								}
								else
									sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
							}
							if(i+1 == gl->glGames->size())
								sock.send_to(boost::asio::buffer(&pom, sizeof(int)), sender_endpoint);
						}
					break;
				case Go:
				case Stop:
				case Left:
				case Right:
				case Take:
				case Open: {
						player *p = getPlayer(clm);
						if(p != nullptr)
							p->command = clm.message;
					}
					break;												
				default:
					break;
			}
			semop(sem_id, &semSignal, 0);
		}
	} 
	catch (std::exception& e) 
	{
        	std::cerr << e.what() << std::endl;
    	}
}

player* serverCommunication::getPlayer(clientMessage msg)
{
	for(int i = 0; i < gl->glGames->size(); i++)
	{
		if(msg.gameID == gl->glGames->at(i).gameID)
		{
			for(int a = 0; a < gl->glGames->at(i).numOfPlayers; a++)
			{
				if(gl->glGames->at(i).players[a].playerID == msg.playerID)
					return &(gl->glGames->at(i).players[a]);
			}
		}
	}
	return nullptr;
}

bool serverCommunication::sendGame(boost::asio::ip::udp::socket *sock, int gameID)
{
	int i;

	if(gl->glGames->size() == 0)
		return false;


	for(i = 0; i < gl->glGames->size(); i++){
		if(gl->glGames->at(i).gameID == gameID)
			break;
		if(i+1 >= gl->glGames->size())
			return false;
	}

	gameplan pom = gl->glGames->at(i);

	for(i = 0; i < pom.numOfPlayers; i++)
	{
		sock->send_to(boost::asio::buffer(&pom, sizeof(gameplan)), pom.players[i].sender_endpoint);
	}
	if(pom.msg == Error)
		return false;
		
	return true;
}

void serverCommunication::loadMaps()
{
	DIR *dp;
	int i = 0;
	struct dirent *ep;     
	dp = opendir ("./examples/");

	if (dp != NULL)
	{
		while (ep = readdir (dp)) 
		{
			if(strcmp(".", ep->d_name) && strcmp("..", ep->d_name))
			{
				mapsInfo mi;
				mi.mazeID = i;
				i++;

				std::ifstream gameMap;
				//open map 
				std::string ss;
				ss = "./examples/";
				ss.append(ep->d_name);

			 	gameMap.open(ss.c_str());
			 	if(!gameMap)
			 	{
			 		std::cerr << "Cant open gameMap" << ep->d_name << std::endl;
			 		continue;
			 	}
			 	std::string line;

				//read first line with size of map
			 	std::getline(gameMap, line);
			 	int row, column;
			 	char x;

				//parse first line to size of map
			 	std::istringstream iss(line);
			 	iss >> row >> x >> column;

			 	if( row > 50 || row < 20 || column > 50 || column < 20)
			 	{
			 		std::cerr << "Bad size of map" << ep->d_name << std::endl;
			 		continue;
			 	}
				gameMap.close();

				mi.width = column;
				mi.height = row;
				mi.mapName = ss;
				maps->push_back(mi);
			}
		}
		(void) closedir (dp);
		std::cout << "There is " << maps->size() << " maps" << std::endl;
	}
	else
		std::cerr << "Couldn't open the directory" << std::endl;
}

/**
 * Funkce, ktera nastavi menici signaly semaforu.
 * @param var Informacni struktura se vsemi dulezetymi informacemi
 * @param semNum Cislo udavajici pocet semaforu
 **/
void serverCommunication::semSet()
{
	semctl(sem_id, 0, SETVAL, 0);

	semWait.sem_num = 0;
	semWait.sem_op = -1;
	semWait.sem_flg = 0;

	semSignal.sem_num = 0;
	semSignal.sem_op = 1;
	semSignal.sem_flg = 0;
}
