
#ifndef basicObj_H
#define basicObj_H

#include <string>
#include <ctime>
#include <boost/asio.hpp>

// Typy prvku bludiste
enum mazeCellType {
		WALL,		// Zed
		PATH,		// Cesta
		GATE_CLOSED,	// Uzavrena brana
		GATE_OPENED,	// Otevrena brana
		FINISH,		// Cil
		KEY		// Klic
		};

// Orientace pohyblivych prvku
enum orientation { UP, DOWN, LEFT, RIGHT};

// Zpravy, ktere muze poslat server a on se na zaklade toho rozhodne co delat
enum clientMsg {NewPlayer,
		SendGames,
		SendMaps,
		CreateGame,
		JoinGame,
		Error,
		Game,
		Go,
		Stop,
		Left,
		Right,
		Take,
		Open};

class mazeCell
{
	public:
		char type;		// Typ pole (jiz primo o co se jedna)
		bool accessible;	// Prostupnost pole
};

class person
{
	public:
		char x;			// X-souradnice
		char y;			// Y-souradnice
		char rotation;		// Natoceni postavy
};

class player: public person
{
	public:
		boost::asio::ip::udp::endpoint sender_endpoint;
		int playerID;		// ID hrace
		int numOfKeys;		// Pocet klicu
		int command;		// Cinnost v dalsim kole
};

class gameInfo
{
	public:
		int gameID;		// ID Hry
		int numOfPlayers;	// Pocet hracu
		time_t gameTime;
};

class mapsInfo
{
	public:
		int width;		// Sirka herniho pole
		int height;		// Vyska herniho pole
		int mazeID;
		std::string mapName;
};

class mapsInfoSend
{
	public:
		int width;		// Sirka herniho pole
		int height;		// Vyska herniho pole
		int mazeID;
};

class gameplan: public mapsInfo, public gameInfo
{
	public:
		gameplan();
		gameplan(int width, int height, int mazeID);
		gameplan(gameInfo gi, mapsInfo mi);
		~gameplan();

		int msg;		// Zprava, pro zjisteni, zda se jeste hraje, nebo je jiz konec
//		int playerID;		// ID hrace

		mazeCell maze[2500];		// Prvky herniho pole

		player players[4];	// Informace o hracich

//		int numOfPoliceman;	// Pocet hlidacu
//		person policeman[10];	// Informace o hlidacich
};

class clientMessage
{
	public:
		int gameID;		// ID Hry
		int playerID;		// ID Hrace
		int message;		// ID Pozadavku
};

#endif
