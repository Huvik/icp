#include "consolePresenter.h"

consolePresenter::consolePresenter(std::string host, std::string port)
{
	cc = new clientCommunication(host, port);
}

consolePresenter::~consolePresenter()
{
	delete cc;
}

void consolePresenter::start()
{
	gameInfo *games = nullptr;
	mapsInfoSend *maps = nullptr;
	int numOf = 0;

	cc->getID();

	std::string command = "nothing";	
	system("clear");
	do {
		switch(checkCommand(command))
		{
			case 0:	break;
			case 1: 
				command = "Refresh";
				do {
					if(command.compare("Refresh"))
					{
						if(!cc->createGame(command))
							std::cout << "Vami zadany prikaz nebyl rozeznan" << std::endl;
						else {
							try {
								std::thread t(&consolePresenter::listenPrint, this);
								readCommands();
								t.join();
							}	
							catch (std::exception& e) 
							{
								std::cerr << e.what() << std::endl;
							}
							break;					
						}
					}
					else
					{
						cc->loadMaps(&maps, &numOf);
					}
					printMaps(numOf, maps);
					std::cin >> command;
					system("clear");
				}while(command.compare("Back"));
				break;
			case 2: 
				command = "Refresh";
				do {
					if(command.compare("Refresh"))
					{
						if(!cc->joinGame(command))
							std::cout << "Pozadovana hra neexistuje, nebo je jiz plna" << std::endl;
						else {
							try {
								std::thread t(&consolePresenter::listenPrint, this);
								readCommands();
								t.join();
							}	
							catch (std::exception& e) 
							{
								std::cerr << e.what() << std::endl;
							}
							
							break;
						}
					}
					else
					{
						cc->loadGames(&games, &numOf);
					}

					printGames(numOf, games);
					std::cin >> command;
					system("clear");				
				}while(command.compare("Back"));
				break;
			case 3: 
				delete cc;
				exit(0);
				break;
			default:
				std::cout << "Vas prikaz nebyl rozeznan" << std::endl << std::endl;
				
		}

		printMenu();
		std::cin >> command;
		system("clear");

	}while(true);

	delete cc;
}

void consolePresenter::printMenu()
{
	using namespace std;
	cout << "--------------------------------------------------" << endl;
	cout << "----------------  Bludiste  2014  ----------------" << endl;
	cout << "--------------------------------------------------" << endl;
	cout << "Zadejte prikaz cinnosti, kterou chcete delat" << endl;
	cout << "POZOR! Prikazy jsou case sensitive!" << endl << endl;
	cout << "NewGame - Umozni vytvorit novou hru" << endl;
	cout << "JoinGame - Umozni pripojeni k jedne z jiz rozehranych her" << endl;
	cout << "Quit - Ukonci program" << endl << endl;
	cout << "Prikaz: ";
}

int consolePresenter::checkCommand(std::string command)
{
	if(!command.compare("nothing"))
		return 0;
	else if(!command.compare("NewGame"))
		return 1;
	else if(!command.compare("JoinGame"))
		return 2;
	else if(!command.compare("Quit"))
		return 3;
	return -1;
}

void consolePresenter::printGames(int numOfGames, gameInfo *games)
{
	using namespace std;
	cout << "--------------------------------------------------" << endl;
	cout << "----------------  Rozehrane  hry  ----------------" << endl;
	cout << "--------------------------------------------------" << endl;
	cout << "gameID --------- Num of players --------- Doba hry" << endl;
	cout << "--------------------------------------------------" << endl;

	if(numOfGames == 0)
		cout << "       Momentalne nejsou zadne rozehrane hry" << endl;
	for(int i = 0; i < numOfGames; i++)
	{
		cout << std::to_string(games[i].gameID) << " --------- ";
		cout << std::to_string(games[i].numOfPlayers) << "/4 --------- ";
		cout << std::to_string(games[i].gameTime) << endl;
	}

	cout << "--------------------------------------------------" << endl;
	cout << "Zadejte prikaz cinnosti, kterou chcete delat" << endl;
	cout << "POZOR! Prikazy jsou case sensitive!" << endl << endl;
	cout << "<gameID> - Umozni pripojeni k jiz rozehrane hre" << endl;
	cout << "Refresh - Znovu nacte rozehrane hry" << endl;
	cout << "Back - Vrati do hlavniho menu" << endl << endl;
	cout << "Prikaz: ";	
}

void consolePresenter::printMaps(int numOfMaps, mapsInfoSend *maps)
{
	using namespace std;
	cout << "--------------------------------------------------" << endl;
	cout << "-------------------  Nová hra  -------------------" << endl;
	cout << "--------------------------------------------------" << endl;
	cout << "bludisteID --------- Sirka --------- Vyska" << endl;
	cout << "--------------------------------------------------" << endl;

	if(numOfMaps == 0)
		cout << "     Momentalne nejsou k dispozici zadne mapy" << endl;
	for(int i = 0; i < numOfMaps; i++)
	{
		cout << std::to_string(maps[i].mazeID) << " --------- ";
		cout << std::to_string(maps[i].width) << " --------- ";
		cout << std::to_string(maps[i].height) <<  endl;
	}

	cout << "--------------------------------------------------" << endl;
	cout << "Zadejte prikaz cinnosti, kterou chcete delat" << endl;
	cout << "POZOR! Prikazy jsou case sensitive!" << endl << endl;
	cout << "<bludisteID> - Zalozi novou hru s mapou tohoto bludiste" << endl;
	cout << "Refresh - Znovu vsechny mapy" << endl;
	cout << "Back - Vrati do hlavniho menu" << endl << endl;
	cout << "Prikaz: ";
}

void consolePresenter::listenPrint()
{
	gameplan *gp = new gameplan();
	do{
		cc->listenGame(gp);
		if(gp->msg == Error)
		{
			std::cerr << "Ouha, neco se pokazilo na serveru, prosim zkuste to pozdeji" << std::endl;
			break;
		}

		using namespace std;/*

		// TODO Zde by se mel udelat vypis hraciho pole

		for(int y = 0; y < gp->height; y++)
		{
			for(int x = 0; x < gp->width; x++)
			{
				switch(gp->maze[gp->height * y + x].type)
				{
					default: cout << ' ';
						break;
				}
			}
			cout << endl;
		}*/
		cout << "Obdrzeli jsme zpravu" << endl;
	}while(true);
}

void consolePresenter::readCommands()
{
	std::string command;
	do
	{
		std::cin >> command;
		cc->sendCommand(command);
	}while(command.compare("leave"));
}
