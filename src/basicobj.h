
#ifndef basicObj_H
#define basicObj_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <ctime>
#include <random>
#include <cmath>
#include <chrono>
#include <thread> 
#include <boost/asio.hpp>

// Typy prvku bludiste
enum mazeCellType {
		WALL,		// Zed
		PATH,		// Cesta
		GATE_CLOSED,	// Uzavrena brana
		FINISH,		// Cil
		KEY,		// Klic
		PERSON 	//Hrac Policeman 
		};

// Orientace pohyblivych prvku
enum orientation { UP, DOWN, LEFT, RIGHT};

// Zpravy, ktere muze poslat server a on se na zaklade toho rozhodne co delat
enum clientMsg {NewPlayer,
		SendGames,
		SendMaps,
		CreateGame,
		JoinGame,
		Error,
		Game,
		Step,
		Go,
		Stop,
		Left,
		Right,
		Take,
		Open,
		Keys};

enum move_result {EMPTY, TAKEN, OCCUPIED, DEAD};

enum players{RED, BLUE, GREEN, YELLOW, POLICEMAN};

class person;


/**
 * Trida pro herni prvky planu rodic 
 * Cesta nebo Zed jinak 
 */
class maze_cell
{
	protected:
		person *active_object;	//aktivni prvek na policku 
		mazeCellType type;	// Typ pole (jiz primo o co se jedna)
		bool accessible;	// Prostupnost pole
	public:

		/**
		 * Vraci hodnotu policka pro cestu je volna
		 */
		virtual move_result empty();

		/**
		 * Vraci hodnotu true pokud jde vzit klic jinak false
		 */
		virtual bool take_key()
		{
			return false;
		}
		/**
		 * Vraci hodnotu true pokud jde brana otevrit jinak false
		 */
		virtual bool open_gate()
		{
			return false;
		}

		/**
		 * Bunka nebo na ni stoji hrac
		 */
		virtual mazeCellType cell_type()
		{
			if(active_object == nullptr)
				return type;
			else
				return PERSON;
		}

		/*
		 * Vraci adresu hrace pro vykresleni
		 */
		person *type_player()
		{
			return active_object;
		}

		/*
		 * Zmeni adresu hrace na zvolenou
		 */
		void changeAddr(person *p)
		{
			active_object = p;
		}
		//Konstruktor
		maze_cell(bool, mazeCellType, person*);
		//Destruktor
		~maze_cell()
		{
			active_object = nullptr;
		}
};

/**
 * Trida pro branu
 */
class gate: public maze_cell
{
	private:
		bool open;
	public:
	// Pokud je brana zavrena nejde vstoupit
	virtual move_result empty()
	{
		if (open)
			return EMPTY;
		else
			return TAKEN;
	}
	// Pokud je brana zavrena tvari se jako brana jinak jako volne pole
	virtual mazeCellType cell_type()
	{
		if(active_object != nullptr)
			return PERSON;
		else if(open)
			return PATH;
		else
			return GATE_CLOSED;
	}
	// Otevreni brany
	virtual bool open_gate()
	{
		if(open)
			return false;
		else
		{
			accessible = true;
			open = true;
			return true;
		} 
	}
	// Konstruktor pro branu
	gate(bool accessible, mazeCellType cell_type, person * active_object):
	maze_cell(accessible, cell_type, active_object)
	{
		open = false;
	}
};

/**
 * Trida pro klic
 */
class key: public maze_cell
{
	private:
		bool took;

	public:
	// Pokud neni klic vzany vraci true
	virtual bool take_key()
	{
		if(!took)
		{
			took = true;
			return true;
		}
		else
			return false;
	}
	//pokud je klic vzany vraci cestu/hrace
	virtual mazeCellType cell_type()
	{
		if(active_object != nullptr)
			return PERSON;
		else if(took)
			return PATH;
		else
			return KEY;
	}
	key(bool accessible, mazeCellType cell_type, person * active_object):
	maze_cell(accessible, cell_type, active_object)
	{
		took = false;
	}
};

/**
 * Trida pro cil
 */
class finish: public maze_cell
{
	public:
		//pokud je cil ukazuje se cil/person
		virtual mazeCellType cell_type()
		{
			if(active_object != nullptr)
				return PERSON;
			else
				return FINISH;
		}
		finish(bool accessible, mazeCellType cell_type, person * active_object):
		maze_cell(accessible, cell_type, active_object){};
};

/** 
* Hlavni trida pro policii, dedi se z ni Hrac
* Zakladni funkce pro ovladani a pohyb policie po hraci plose
*/
class person
{
	protected:
		unsigned int x;
		unsigned int y;
		orientation faceOrientation;
		players typePlayer;
		maze_cell ***mapa;

	public:
		//Konstruktor pro policii
		person(maze_cell*** mapa, unsigned int x, unsigned int y);
		/**
		 * Vraci natoceni postavy
		 * @return natoceni postavy {UP, DOWN, LEFT, RIGHT}
		 */
		orientation returnOrientation()
		{
			return faceOrientation;
		}
		
		/**
		 * Vraci typ hrace
		 * @return  {RED, BLUE, GREEN, YELLOW, POLICEMAN}
		 */		
		players returnPlayer()
		{
			return typePlayer;
		}
		/**
		 * Virtualni metoda pro krok
		 * @return  TRUE/FALSE pokud je mozne udelat krok nebo ne
		 */
		virtual bool step();
		/**
		 * Virtualni metoda pro soubezny pohyb
		 * @return  TRUE/FALSE pokud je mozne udelat krok nebo ne
		 */		
		virtual bool move();

		/**
		 * Virtualni metoda pro zabijeni policie a hrace
		 */
		virtual void kill()
		{
			std::cerr << "Try to kill police!";
		}
		person(){};
};


/**
 * Trida pro ovladani hrace a manipulaci s nim
 */
class player: public person
{
	private:
		bool alive;
		unsigned int steps;
		unsigned int keys;
		bool moving;
		boost::asio::ip::udp::endpoint sender_endpoint;
		int playerID;
	public:
		bool openGate();
		bool pickKey();
		player(){};
		/**
		 * Zabiti hrace
		 */
		virtual void kill()
		{
			alive = false;
		}
		/**
		 * Otoceni hrace do prava
		 */
		void right();
		/**
			* Otoceni hrace do leva
			*/
		void left();
		/**
		 * Virtualni metoda pro krok
		 * @return  TRUE/FALSE pokud je mozne udelat krok nebo ne
		 */
		virtual bool step();
		/**
 		 * Vraci uslych kroku
 		 * @return  pocet uslych kroku
 		 */
		int retSteps();
		/** 
		 * Nastavi edpoint pro hrace, kde se maji posilat data
		 */
		void setEndpoint(boost::asio::ip::udp::endpoint point)
		{
			sender_endpoint = point;
		}
		/**
		 * Vraci endpoint od clienta pro posilani dat
		 * @return sender_endpoint boost::asio::ip::udp::endpoint
		 */
		boost::asio::ip::udp::endpoint retEndpoint()
		{
			return sender_endpoint;
		}
		/**
		 * Umisteni hrace na hraci plochu a pripojeni do hry
		 */
		void joinGame(int ID, players p, unsigned int xSize, unsigned int ySize, maze_cell ***m);
		/**
		 * Vraci hodnotu id od hrace
		 * @return playerID hodnota hracoveho id
		 */
		int retPlayerid()
		{
			return playerID;
		}
		int retKeys()
		{
			return keys;
		}
		void stop()
		{
			moving = false;
		}

		void go()
		{
			moving = true;
		}

		virtual bool move()
		{
			if(moving)
				this->step();
		}
};






class gameInfo
{
	public:
		int gameID;		// ID Hry
		int numOfPlayers;	// Pocet hracu
		time_t gameTime;
		int step;
};

class mapsInfo
{
	public:
		int width;		// Sirka herniho pole
		int height;		// Vyska herniho pole
		int mazeID;
		std::string mapName;
};

class mapsInfoSend
{
	public:
		int width;		// Sirka herniho pole
		int height;		// Vyska herniho pole
		int mazeID;
};




class game
{
	private:
		unsigned int width, height;
		maze_cell ***maze;
		std::vector<person*> police;
		char *buffer;
	public:
		game(unsigned int, unsigned int, std::ifstream&, char*);
		
		/**
 		* Pri kazdem kole pohybuje s kazdym policajtem po mape
 		*/
		void movePolice();

		/**
		 * Kopiruje vytvorenou mapu do bufferu
		 */
		void createMap(char *buffer);

		maze_cell ***m()
		{
			return maze;
		}
};




class gameplan: public mapsInfo, public gameInfo
{
	public:
		gameplan(){};
		gameplan(gameInfo gi, mapsInfo mi);

		game *gameLogic;
		int msg;		// Zprava, pro zjisteni, zda se jeste hraje, nebo je jiz konec
		char maze[2500];		// Prvky herniho pole

		player *players;	// Informace o hracich
};





class clientMessage
{
	public:
		int gameID;		// ID Hry
		int playerID;		// ID Hrace
		int message;		// ID Pozadavku
		int step;
};

#endif
