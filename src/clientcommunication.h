#ifndef clientCommunication_H
#define clientCommunication_H

#include <boost/asio.hpp>
#include <stdlib.h>
#include <cstdlib>

#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>

#include "basicobj.h"

class clientCommunication
{
    private:
	// Semafory
        int sem_id;
        struct sembuf semWait;
        struct sembuf semSignal;

	// Komunikace se serverem
        boost::asio::ip::udp::socket *socket;
        boost::asio::ip::udp::endpoint *receiver_endpoint;
    public:
	// Pomocne promenne
        int playerID;
        int gameID;

	// Construktory, Destruktory
        clientCommunication();
        clientCommunication(std::string ip, std::string port);
        ~clientCommunication();

	// Ostatni
        void connect(std::string ip, std::string port);
        void getID();
        bool createGame(std::string mapID, std::string step);
	bool createGame(int mapID, int step);
	bool joinGame(std::string gameID);
        bool joinGame(int gameID);
        void loadGames(gameInfo **games, int *numOfGames);
        void loadMaps(mapsInfoSend **maps, int *numOfMaps);
        void createGame();
        void listenGame(gameplan *gp);
        void sendCommand(std::string command);
        void semSet();
};

#endif

