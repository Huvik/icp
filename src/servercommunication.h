/**
 * Projekt Bludiste do predmetu ICP 2013/2014
 * Authors: Jan Trcka - xtrcka07 (xtrcka07@stud.fit.vutbr.cz, trcka.j@gmail.com)
 *          Lukas Huvar - xhuvar00 (xhuvar00@stud.fit.vutbr.cz)
 * Date: 11. 05. 2014
 * Description: Trida pro obsluhu a komunikaci ze strany serveru
 **/

#ifndef serverCommunication_H
#define serverCommunication_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <boost/asio.hpp>
#include <thread> 
#include <dirent.h>
#include <sys/types.h>
#include <chrono>

// Semafory
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>

#include "basicobj.h"
#include "gamelogic.h"

class serverCommunication
{
	private:
		// Semafory
		int sem_id;
		struct sembuf semWait;
		struct sembuf semSignal;
	public:
		// Spolecne promenne
		gameLogic *gl;
		int pomPlayerID;
		int pomGameID;
		std::vector<gameInfo>* games;
		std::vector<mapsInfo>* maps;

		// Konstruktor
		serverCommunication(std::string port);

		// Funkce
		void loadMaps();
		void newThr(boost::asio::ip::udp::endpoint sender_endpoint, boost::asio::ip::udp::socket *sock, clientMessage clm);
		bool sendGame(boost::asio::ip::udp::socket *sock, int gameID);
		void semSet();
		player* getPlayer(clientMessage msg);
};

#endif
