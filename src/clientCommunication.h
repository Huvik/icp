#ifndef clientCommunication_H
#define clientCommunication_H

#include <boost/asio.hpp>
#include <mutex>

#include "basicObj.h"
#include "gameLogic.h"

class clientCommunication
{
	private:
		boost::asio::ip::udp::socket *socket;
		boost::asio::ip::udp::endpoint *receiver_endpoint;
	public:
		std::mutex socket_LOCK; 
		int playerID;
		int gameID;
		clientCommunication();
		clientCommunication(std::string ip, std::string port);
		~clientCommunication();
		void connect(std::string ip, std::string port);
		void getID();
		bool createGame(std::string mapID);
		bool joinGame(std::string gameID);
		void loadGames(gameInfo **games, int *numOfGames);
		void loadMaps(mapsInfoSend **maps, int *numOfMaps);
		void createGame();
		void listenGame(gameplan *gp);
		void sendCommand(std::string command);
};

#endif
