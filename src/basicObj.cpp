#include "basicObj.h"

gameplan::gameplan()
{

}

gameplan::gameplan(int width, int height, int mazeID)
{
	this->width = width;
	this->height = height;
	this->mazeID = mazeID;
}

gameplan::gameplan(gameInfo gi, mapsInfo mi)
{
	this->gameID = gi.gameID;
	this->numOfPlayers = gi.numOfPlayers;
	this->gameTime = gi.gameTime;

	this->width = mi.width;
	this->height = mi.height;
	this->mazeID = mi.mazeID;
	this->mapName = mi.mapName;
}

gameplan::~gameplan()
{

}
