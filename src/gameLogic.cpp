#include "gameLogic.h"

bool gameLogic::joinGame(int gameID)
{

}

int gameLogic::createGame(gameInfo gi, mapsInfo mi, boost::asio::ip::udp::endpoint sender_endpoint)
{
	gameplan gp(gi, mi);
	gp.msg = Game;

	gp.players[0].sender_endpoint = sender_endpoint;

	std::ifstream gameMap;
	gameMap.open(mi.mapName);

 	if(!gameMap) {
 		std::cerr << "Cant open gameMap" << mi.mapName << std::endl;
		gp.msg = Error;
	}
	else
	{
		char c;
		std::string pom;
		getline(gameMap, pom);

		for(int i = 0; i < (gp.width * gp.height); i++)
		{
			gameMap >> c;
			switch(c)
			{
				// TODO Do struktury sptavne nacist znaky ze souboru
				case EOF:
					std::cerr << "Incomplete maze " << mi.mapName << std::endl;
					gp.msg = Error;
					return 0;
					break;
				default:
					gp.maze[i].type = PATH;
			}			
		}
		gameMap.close();
	}

	this->glGames->push_back(gp);

	return gp.gameID;
}

void gameLogic::playGame(int gameID)
{
	// Toto by ti melo zpristupnit data dane hry
	gameplan *pom = this->glGames[gameID].data();

	// TODO Zde by se mely provest vyposty noveho "kola", o odeslani jsem se uz postaral
}

gameLogic::gameLogic()
{
	glGames = new std::vector<gameplan>();
}

gameLogic::~gameLogic()
{
	delete glGames;
}
