CC = g++
CFLAGS = -std=c++11
LIBS = -lpthread -lboost_system -lboost_thread
SRC = ./src/
OBJ = ./obj/

ALL: bludiste2014-server bludiste2014-cli

bludiste2014-server: $(SRC)bludiste2014-server.cpp $(SRC)servercommunication.h $(OBJ)servercommunication.o $(OBJ)gamelogic.o $(OBJ)basicobj.o
	$(CC) $(CFLAGS) $(LIBS) $(SRC)bludiste2014-server.cpp $(OBJ)servercommunication.o $(OBJ)gamelogic.o $(OBJ)basicobj.o -o bludiste2014-server

bludiste2014-cli: $(SRC)bludiste2014-cli.cpp $(OBJ)clientcommunication.o $(OBJ)consolepresenter.o $(OBJ)basicobj.o
	$(CC) $(CFLAGS) $(LIBS) $(SRC)bludiste2014-cli.cpp $(OBJ)clientcommunication.o $(OBJ)consolepresenter.o $(OBJ)basicobj.o -o bludiste2014-cli

bludiste2014:
	cd src/qt/

$(OBJ)consolepresenter.o: $(SRC)consolepresenter.cpp $(SRC)consolepresenter.h
	$(CC) $(CFLAGS) $(LIBS) $(SRC)consolepresenter.cpp -c
	mv consolepresenter.o $(OBJ)

$(OBJ)basicobj.o: $(SRC)basicobj.cpp $(SRC)basicobj.h
	$(CC) $(CFLAGS) $(LIBS) $(SRC)basicobj.cpp -c
	mv basicobj.o $(OBJ)

$(OBJ)clientcommunication.o: $(SRC)clientcommunication.cpp $(SRC)clientcommunication.h
	$(CC) $(CFLAGS) $(LIBS) $(SRC)clientcommunication.cpp -c
	mv clientcommunication.o $(OBJ)

$(OBJ)servercommunication.o: $(SRC)servercommunication.cpp $(SRC)servercommunication.h $(SRC)gamelogic.h
	$(CC) $(CFLAGS) $(LIBS) $(SRC)servercommunication.cpp $(SRC)servercommunication.h -c
	mv servercommunication.o $(OBJ)

$(OBJ)gamelogic.o: $(SRC)gamelogic.cpp $(SRC)gamelogic.h
	$(CC) $(CFLAGS) $(LIBS) $(SRC)gamelogic.cpp $(SRC)gamelogic.h -c
	mv gamelogic.o $(OBJ)

clean:
	rm $(OBJ)*.o $(SRC)*.gch bludiste2014-server bludiste2014 bludiste2014-cli
